<?php

namespace Drupal\external_entities_test\Controller;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * An simple JSON controller.
 *
 * This controller mimics a basic REST endpoint that can be queried for:
 * - listing: URI(GET)=external-entities-test/{dataset}
 *   Listing supports basic filtering (ie. field_name=value or
 *   field_name=value1,value2,value3 to match at least one of a set of values)
 *   and pagging using 'page' for page number starting from 0 and 'pageSize' for
 *   page size (default to 50).
 * - getting an entity: URI(GET)=external-entities-test/{dataset}/{entity_id}
 * - updating/creating an entity:
 *   URI(POST/PUT)=external-entities-test/{dataset}/{entity_id}
 *
 * Datasets can be added through setRawData($dataset, $data) where $dataset is a
 * dataset name and $data is an array of raw entities keyed by their identifier.
 * Ex.:
 * @code
 *   $xntt_json_controller = $this->container->get('controller_resolver')->getControllerFromDefinition('\Drupal\external_entities_test\Controller\ExternalEntitiesJsonController::listItems')[0];
 *   $xntt_json_controller->setRawData('ref', [
 *     'ref1' => [
 *       'uuid' => 'ref1',
 *       'label' => 'Term 1',
 *     ],
 *     'ref2' => [
 *       'uuid' => 'ref2',
 *       'label' => 'Term 2',
 *     ],
 *   ]);
 * @endcode
 */
class ExternalEntitiesJsonController extends ControllerBase {

  /**
   * Page size.
   */
  const PAGE_SIZE = 50;

  /**
   * Get data from a dataset.
   */
  public function getRawData(string $dataset) :array {
    $datasets = $this->state()->get('external_entities_test.data') ?? [];
    return $datasets[$dataset] ?? [];
  }

  /**
   * Set a dataset.
   */
  public function setRawData(string $dataset, array $data = []) {
    $datasets = $this->state()->get('external_entities_test.data') ?? [];
    if (!empty($data) || empty($datasets[$dataset])) {
      $datasets[$dataset] = $data;
    }
    $this->state()->set('external_entities_test.data', $datasets);
    return $datasets[$dataset];
  }

  /**
   * Returns the total number of items for the given dataset as JSON.
   */
  public function countItems(string $dataset, Request $request) {
    $data = $this->getRawData($dataset);
    $response = new CacheableJsonResponse(['count' => count($data)], 200);
    return $response;
  }

  /**
   * Returns a list of items for the given dataset as JSON.
   */
  public function listItems(string $dataset, Request $request) {
    // Set the default cache.
    $cache = new CacheableMetadata();
    $cache->addCacheTags([
      'external_entities_test',
      'external_entities_test_' . $dataset,
    ]);

    // Add the cache contexts for the request parameters.
    $cache->addCacheContexts([
      'url',
      'url.query_args',
    ]);

    $data = $this->getRawData($dataset);

    // Process parameters.
    $params = $request->query->all();
    // Pagging.
    $page_size = static::PAGE_SIZE;
    if (!empty($params['pageSize']) && is_numeric($params['pageSize'])) {
      $page_size = $params['pageSize'];
      unset($params['pageSize']);
    }
    $page = 0;
    if (isset($params['page']) && is_numeric($params['page'])) {
      $page = $params['page'];
      unset($params['page']);
    }

    // Check for filters.
    if (!empty($params)) {
      foreach ($params as $field => $values) {
        if (!is_array($values)) {
          $values = explode(',', $values);
        }
        foreach ($data as $id => $record) {
          if (!in_array($record[$field], $values)) {
            unset($data[$id]);
          }
        }
      }
    }

    // Pagging management.
    $start = $page * $page_size;
    $data = array_slice($data, $start, $page_size);

    $response = new CacheableJsonResponse(array_values($data), 200);
    $response->addCacheableDependency($cache);
    return $response;
  }

  /**
   * Returns the requested item as JSON.
   */
  public function getItem(string $dataset, string $uuid, Request $request) {
    $data = $this->getRawData($dataset);
    if (!isset($data[$uuid])) {
      return new JsonResponse([], 404);
    }

    // Set the default cache.
    $cache = new CacheableMetadata();
    $cache->addCacheTags([
      'external_entities_test',
      'external_entities_test_' . $dataset,
      'external_entities_test_' . $dataset . ':' . $uuid,
    ]);

    // Add the cache contexts for the request parameters.
    $cache->addCacheContexts([
      'url',
    ]);

    $response = new CacheableJsonResponse($data[$uuid], 200);
    $response->addCacheableDependency($cache);
    return $response;
  }

  /**
   * Sets a given item.
   */
  public function setItem(string $dataset, string $uuid, Request $request) {
    $data = $this->getRawData($dataset);
    if (!isset($data[$uuid])) {
      return new JsonResponse([], 404);
    }
    $params = $this->getRequestContent($request);
    $data[$uuid] = $params;
    $this->setRawData($dataset, $data);

    // Invalidate cache.
    Cache::invalidateTags([
      'external_entities_test',
      'external_entities_test_' . $dataset,
      'external_entities_test_' . $dataset . ':' . $uuid,
    ]);

    $response = new JsonResponse($data[$uuid], 200);
    return $response;
  }

  /**
   * Get the request content.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   API Request.
   *
   * @return array
   *   Request content.
   */
  public function getRequestContent(Request $request) {
    $body = $request->getContent();
    $content = \Drupal::service('external_entities.response_decoder_factory')
      ->getDecoder('json')
      ->decode($body);

    // parse_str($request->getContent(), $content);.
    return $content;
  }

}
