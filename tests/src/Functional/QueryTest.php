<?php

namespace Drupal\Tests\external_entities\Functional;

use Drupal\Component\Serialization\Json;

/**
 * Test external entities query system.
 *
 * @group ExternalEntities
 */
class QueryTest extends ExternalEntitiesBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'external_entities',
    'system',
    'field',
    'text',
  ];

  /**
   * Test container.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  protected $container;

  /**
   * The entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Raw data.
   *
   * @var array
   */
  protected $rawData;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    global $base_url;

    $this->entityTypeManager = $this->container->get('entity_type.manager');
    $this->storage = $this->entityTypeManager->getStorage('external_entity_type');

    // Setup datasets.
    $xntt_json_controller = $this
      ->container
      ->get('controller_resolver')
      ->getControllerFromDefinition(
        '\Drupal\external_entities_test\Controller\ExternalEntitiesJsonController::listItems'
      )[0];

    $this->rawData = [
      1001 => [
        'id' => 1001,
        'title' => 'Instance 1',
        'data_field_a' => 'toto',
        'data_field_b' => 42,
        'data_field_c' => ['abc', 'def', 'ghi'],
        'data_field_d' => [
          'sub_field_d1' => 'A',
          'sub_field_d2' => 'B',
        ],
      ],
      1002 => [
        'id' => 1002,
        'title' => 'Instance 2',
        'data_field_a' => 'tata',
        'data_field_b' => 501,
        'data_field_c' => ['abc', 'def'],
        'data_field_d' => [
          'sub_field_d1' => 'A',
          'sub_field_d2' => 'B',
        ],
      ],
      1003 => [
        'id' => 1003,
        'title' => 'Instance 3',
        'data_field_a' => 'tete',
        'data_field_b' => 42,
        'data_field_c' => ['abc'],
        'data_field_d' => [
          'sub_field_d1' => 'A',
          'sub_field_d2' => 'B',
        ],
      ],
      1004 => [
        'id' => 1004,
        'title' => 'Instance 4',
        'data_field_a' => 'titi',
        'data_field_b' => 806,
        'data_field_c' => ['def', 'ghi'],
        'data_field_d' => [
          'sub_field_d1' => 'C',
          'sub_field_d2' => 'C',
        ],
      ],
      1005 => [
        'id' => 1005,
        'title' => 'Instance 5',
        'data_field_a' => 'tutu',
        'data_field_b' => 77,
        'data_field_c' => ['ghi'],
        'data_field_d' => [
          'sub_field_d1' => 'D',
          'sub_field_d2' => 'E',
        ],
      ],
      1006 => [
        'id' => 1006,
        'title' => 'Instance 6',
        'data_field_a' => 'tota',
        'data_field_b' => 64,
        'data_field_c' => ['def'],
        'data_field_d' => [
          'sub_field_d1' => 'B',
          'sub_field_d2' => 'A',
        ],
      ],
      1007 => [
        'id' => 1007,
        'title' => 'Instance 7',
        'data_field_a' => 'tote',
        'data_field_b' => 51,
        'data_field_c' => ['xyz'],
        'data_field_d' => [
          'sub_field_d1' => '',
          'sub_field_d2' => NULL,
        ],
      ],
      1008 => [
        'id' => 1008,
        'title' => 'Instance 8',
        'data_field_a' => 'toti',
        'data_field_b' => 42,
        'data_field_c' => ['ghi', 'def', 'abc'],
        'data_field_d' => [
          'sub_field_d1' => 'AX',
          'sub_field_d2' => 'BX',
        ],
      ],
      1009 => [
        'id' => 1009,
        'title' => 'Instance 9',
        'data_field_a' => 'totu',
        'data_field_b' => 34,
        'data_field_c' => ['abc', 'def', 'ghi', 'xyz', 'abc', 'def'],
        'data_field_d' => [
          'sub_field_d1' => 'a',
          'sub_field_d2' => 'b',
        ],
      ],
      1010 => [
        'id' => 1010,
        'title' => 'Instance 10',
        'data_field_a' => 'foo',
        'data_field_b' => 21,
        'data_field_c' => [],
        'data_field_d' => [
          'sub_field_d1' => 'X',
          'sub_field_d2' => 'X',
        ],
      ],
      1011 => [
        'id' => 1011,
        'title' => 'Instance 11',
        'data_field_a' => 'bar',
        'data_field_b' => 16,
        'data_field_c' => [NULL],
        'data_field_d' => [
          'sub_field_d1' => 'A',
          'sub_field_d2' => 'B',
        ],
      ],
      1012 => [
        'id' => 1012,
        'title' => 'Instance 12',
        'data_field_a' => 'baz',
        'data_field_b' => 806,
        'data_field_c' => ['abcdefghi'],
        'data_field_d' => [
          'sub_field_d1' => 'C',
          'sub_field_d2' => 'D',
        ],
      ],
    ];

    $xntt_json_controller->setRawData('simple', $this->rawData);

    $type = $this->storage->create([
      'id' => 'simple_external_entity',
      'label' => 'Simple external entity',
      'label_plural' => 'Simple external entities',
      'description' => '',
      'generate_aliases' => FALSE,
      'read_only' => FALSE,
      'debug_level' => 0,
      'field_mappers' => [],
      'storage_clients' => [],
      'data_aggregator' => [],
      'persistent_cache_max_age' => 0,
    ]);
    // Sets aggregator.
    $type->setDataAggregatorId('single')->setDataAggregatorConfig([
      'storage_clients' => [
        [
          'id' => 'rest',
          'config' => [
            'endpoint' => $base_url . '/external-entities-test/simple',
            'endpoint_options' => [
              'single' => '',
              'count' => count($this->rawData),
              'count_mode' => NULL,
              'cache' => FALSE,
              'limit_qcount' => 0,
              'limit_qtime' => 0,
            ],
            'response_format' => 'json',
            'data_path' => [
              'list' => '',
              'single' => '',
              'keyed_by_id' => FALSE,
              'count' => '',
            ],
            'pager' => [
              'default_limit' => count($this->rawData),
              'type' => 'pagination',
              'page_parameter' => 'page',
              'page_parameter_type' => 'pagenum',
              'page_start_one' => FALSE,
              'page_size_parameter' => 'pageSize',
              'page_size_parameter_type' => 'pagesize',
            ],
            'api_key' => [
              'type' => 'none',
              'header_name' => '',
              'key' => '',
            ],
            'http' => [
              'headers' => '',
            ],
            'parameters' => [
              'list' => [],
              'list_param_mode' => 'query',
              'single' => [],
              'single_param_mode' => 'query',
            ],
            'filtering' => [
              // We need to enable Drupal-side filtering to have full filtering.
              'drupal' => TRUE,
              'basic' => TRUE,
              'basic_fields' => [],
              'list_support' => 'none',
              'list_join' => '',
            ],
          ],
        ],
      ],
    ]);
    $type->save();

    // Add fields.
    $this
      ->createField('simple_external_entity', 'field_alpha', 'string')
      ->createField('simple_external_entity', 'field_beta', 'integer')
      ->createField('simple_external_entity', 'field_gama', 'string');

    // Set field mappers...
    $type
      // ID field mapping.
      ->setFieldMapperId('id', 'generic')->setFieldMapperConfig(
        'id',
        [
          'property_mappings' => [
            'value' => [
              'id' => 'direct',
              'config' => [
                'mapping' => 'id',
                'required_field' => TRUE,
                'main_property' => TRUE,
                'data_processors' => [],
              ],
            ],
          ],
          'debug_level' => 0,
        ]
      )
      // UUID field mapping.
      ->setFieldMapperId('uuid', 'generic')
      ->setFieldMapperConfig(
        'uuid',
        [
          'property_mappings' => [
            'value' => [
              'id' => 'direct',
              'config' => [
                'mapping' => 'id',
                'required_field' => FALSE,
                'main_property' => TRUE,
                'data_processors' => [],
              ],
            ],
          ],
          'debug_level' => 0,
        ]
      )
      // Title field mapping.
      ->setFieldMapperId('title', 'generic')
      ->setFieldMapperConfig(
        'title',
        [
          'property_mappings' => [
            'value' => [
              'id' => 'direct',
              'config' => [
                'mapping' => 'title',
                'required_field' => TRUE,
                'main_property' => TRUE,
                'data_processors' => [],
              ],
            ],
          ],
          'debug_level' => 0,
        ]
      )
      // Alpha field mapping.
      ->setFieldMapperId('field_alpha', 'generic')
      ->setFieldMapperConfig(
        'field_alpha',
        [
          'property_mappings' => [
            'value' => [
              'id' => 'direct',
              'config' => [
                'mapping' => 'data_field_a',
                'required_field' => TRUE,
                'main_property' => TRUE,
                'data_processors' => [],
              ],
            ],
          ],
          'debug_level' => 0,
        ]
      )
      // Beta field mapping.
      ->setFieldMapperId('field_beta', 'generic')
      ->setFieldMapperConfig(
        'field_beta',
        [
          'property_mappings' => [
            'value' => [
              'id' => 'direct',
              'config' => [
                'mapping' => 'data_field_b',
                'required_field' => TRUE,
                'main_property' => TRUE,
                'data_processors' => [],
              ],
            ],
          ],
          'debug_level' => 0,
        ]
      )
      // Gama field mapping.
      ->setFieldMapperId('field_gama', 'generic')
      ->setFieldMapperConfig(
        'field_gama',
        [
          'property_mappings' => [
            'value' => [
              'id' => 'direct',
              'config' => [
                'mapping' => 'data_field_c',
                'required_field' => TRUE,
                'main_property' => TRUE,
                'data_processors' => [],
              ],
            ],
          ],
          'debug_level' => 0,
        ]
      );
    $type->save();
  }

  /**
   * Test external entity query system.
   */
  public function testQuery() {

    // Check "simple test endpoint".
    $simple_json = Json::decode($this->drupalGet('external-entities-test/simple'));
    $this
      ->assertSession()
      ->statusCodeEquals(200);
    $this
      ->assertCount(count($this->rawData), $simple_json);
    $this
      ->assertSession()
      ->responseHeaderEquals('Content-Type', 'application/json');

    // Basic query, get all.
    $query = $this->entityTypeManager->getStorage('simple_external_entity')->getQuery();
    $result = $query->accessCheck(FALSE)->execute();
    $this->assertEquals(
      [
        1001 => 1001,
        1002 => 1002,
        1003 => 1003,
        1004 => 1004,
        1005 => 1005,
        1006 => 1006,
        1007 => 1007,
        1008 => 1008,
        1009 => 1009,
        1010 => 1010,
        1011 => 1011,
        1012 => 1012,
      ],
      $result,
      'basic query'
    );

    // Basic query, get the first 3.
    $query = $this->entityTypeManager->getStorage('simple_external_entity')->getQuery();
    $result = $query->pager(3)->accessCheck(FALSE)->execute();
    $this->assertEquals(
      [
        1001 => 1001,
        1002 => 1002,
        1003 => 1003,
      ],
      $result,
      'basic query with pager(3)'
    );

    // Basic query with title = Instance 4.
    $query = $this->entityTypeManager->getStorage('simple_external_entity')->getQuery();
    $result = $query->condition('title', 'Instance 4')->accessCheck(FALSE)->execute();
    $this->assertEquals(
      [
        1004 => 1004,
      ],
      $result,
      'basic query with title = Instance 4'
    );

    // Basic query with field_alpha ending by "ta".
    $query = $this->entityTypeManager->getStorage('simple_external_entity')->getQuery();
    $result = $query->condition('field_alpha', 'ta', 'ENDS_WITH')->accessCheck(FALSE)->execute();
    $this->assertEquals(
      [
        1002 => 1002,
        1006 => 1006,
      ],
      $result,
      'basic query with field_alpha ending by ta'
    );

    // Query with field_alpha ending by "ta" or starting with "te".
    $query = $this->entityTypeManager->getStorage('simple_external_entity')->getQuery();
    $or = $query->orConditionGroup()->condition('field_alpha', 'ta', 'ENDS_WITH')->condition('field_alpha', 'te', 'STARTS_WITH');
    $result = $query->condition($or)->accessCheck(FALSE)->execute();
    $this->assertEquals(
      [
        1002 => 1002,
        1003 => 1003,
        1006 => 1006,
      ],
      $result,
      'query with field_alpha ending by ta or starting with te'
    );

    // Query with field_alpha ending by "ta" or starting with "te" and
    // field_beta != 64.
    $query = $this->entityTypeManager->getStorage('simple_external_entity')->getQuery();
    $or = $query->orConditionGroup()->condition('field_alpha', 'ta', 'ENDS_WITH')->condition('field_alpha', 'te', 'STARTS_WITH');
    $result = $query->condition('field_beta', 64, '<>')->condition($or)->accessCheck(FALSE)->execute();
    $this->assertEquals(
      [
        1002 => 1002,
        1003 => 1003,
      ],
      $result,
      'query with field_alpha ending by ta or starting with te and field_beta != 64'
    );

    // Complex query 1.
    $query = $this->entityTypeManager->getStorage('simple_external_entity')->getQuery();
    $and1 = $query->andConditionGroup()->condition('field_beta', [16, 42, 51], 'IN')->condition('field_alpha', 'ti', 'CONTAINS');
    $and2 = $query->andConditionGroup()->condition('field_beta', [30, 50], 'BETWEEN')->condition('field_gama', 'xyz');
    $or = $query->orConditionGroup()->condition($and1)->condition($and2);
    $result = $query->condition('field_alpha', 'to', 'STARTS_WITH')->condition($or)->accessCheck(FALSE)->execute();
    $this->assertEquals(
      [
        1008 => 1008,
        1009 => 1009,
      ],
      $result,
      'complex query 1'
    );
  }

}
