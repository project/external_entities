<?php

namespace Drupal\Tests\external_entities\Unit;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\external_entities\Plugin\ExternalEntities\DataProcessor\Filter;

/**
 * The class to test value mapping data processor.
 *
 * @group ExternalEntities
 */
class DataProcessorFilterUnitTest extends UnitTestCase {

  /**
   * Data provider for testFilterProcessor().
   *
   * Structure:
   * - data
   * - expected_result
   * - config
   * - test name.
   */
  public static function provideTestFilterProcessor() {
    return [
      // NULL filtering.
      [
        ['toto', NULL, 'titi', '', 'toto', 'tata', NULL, 'tutu', 'tete', '0', NULL, 'tototyty', 0, 'blitata', NULL],
        ['toto', 'titi', '', 'toto', 'tata', 'tutu', 'tete', '0', 'tototyty', 0, 'blitata'],
        [
          'filtered' => [],
          'filter_empty' => 'null',
        ],
        'Null filtering test',
      ],
      // Value filtering.
      [
        ['toto', NULL, 'titi', '', 'toto', 'tata', NULL, 'tutu', 'tete', '0', NULL, 'tototyty', 0, 'blitata', NULL],
        [NULL, 'titi', '', 'tata', NULL, 'tutu', '0', NULL, 'tototyty', 0, 'blitata', NULL],
        [
          'filtered' => [
            'toto' => 'remove',
            'tete' => 'remove',
          ],
          'filter_empty' => '',
        ],
        'Value filtering test',
      ],
      // Null and value filtering.
      [
        ['toto', NULL, 'titi', '', 'toto', 'tata', NULL, 'tutu', 'tete', '0', NULL, 'tototyty', 0, 'blitata', NULL],
        ['titi', '', 'tata', 'tutu', '0', 'tototyty', 0, 'blitata'],
        [
          'filtered' => [
            'toto' => 'remove',
            'tete' => 'remove',
          ],
          'filter_empty' => 'null',
        ],
        'Null and value filtering test',
      ],
      // Empty and value filtering.
      [
        ['toto', NULL, 'titi', '', 'toto', 'tata', NULL, 'tutu', 'tete', '0', NULL, 'tototyty', 0, 'blitata', NULL],
        ['titi', 'tata', 'tutu', 'tototyty', 'blitata'],
        [
          'filtered' => [
            'toto' => 'remove',
            'tete' => 'remove',
          ],
          'filter_empty' => 'empty',
        ],
        'Empty and value filtering test',
      ],
      // Empty and value filtering, keeping 0s.
      [
        ['toto', NULL, 'titi', '', 'toto', 'tata', NULL, 'tutu', 'tete', '0', NULL, 'tototyty', 0, 'blitata', NULL],
        ['titi', 'tata', 'tutu', '0', 'tototyty', 0, 'blitata'],
        [
          'filtered' => [
            'toto' => 'remove',
            'tete' => 'remove',
          ],
          'filter_empty' => 'zerook',
        ],
        'Empty and value filtering keeping 0s test',
      ],
    ];
  }

  /**
   * Tests value mapping data processor.
   *
   * @dataProvider provideTestFilterProcessor
   */
  public function testFilterProcessor(
    array $data,
    array $expected_result,
    array $config,
    string $test_name,
  ) {

    $string_translation = $this->getMockBuilder(TranslationInterface::class)
      ->getMock();

    $logger = $this->getMockBuilder(LoggerChannelInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $logger_factory = $this->getMockBuilder(LoggerChannelFactoryInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $logger_factory
      ->expects($this->any())
      ->method('get')
      ->with('xntt_data_processor_filter')
      ->willReturn($logger);

    $field_def = $this->getMockBuilder(FieldDefinitionInterface::class)
      ->getMock();

    $data_processor = new Filter($config, 'filter', [], $string_translation, $logger_factory);

    $result = $data_processor->processData($data, $field_def, 'value');
    $this->assertEquals($expected_result, $result, $test_name);
  }

}
