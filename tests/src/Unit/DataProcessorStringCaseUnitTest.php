<?php

namespace Drupal\Tests\external_entities\Unit;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\external_entities\Plugin\ExternalEntities\DataProcessor\StringCase;

/**
 * The class to test string case changer data processor.
 *
 * @group ExternalEntities
 */
class DataProcessorStringCaseUnitTest extends UnitTestCase {

  /**
   * Data provider for testStringCaseProcessor().
   *
   * Structure:
   * - data
   * - expected_result
   * - config
   * - test name.
   */
  public static function provideTestStringCaseProcessor() {
    return [
      // No change.
      [
        ['text1', 'Text2', 'TEXT3', 'tEXT4', 'Some other text', '1 more "Te_xt".'],
        ['text1', 'Text2', 'TEXT3', 'tEXT4', 'Some other text', '1 more "Te_xt".'],
        [
          'format' => '',
          'non_word' => '',
        ],
        'No change test',
      ],
      // To upper case.
      [
        ['text1', 'Text2', 'TEXT3', 'tEXT4', 'Some other text', '1 more "Te_xt".'],
        ['TEXT1', 'TEXT2', 'TEXT3', 'TEXT4', 'SOME OTHER TEXT', '1 MORE "TE_XT".'],
        [
          'format' => 'upper',
          'non_word' => '',
        ],
        'To upper case test',
      ],
      // To lower case.
      [
        ['text1', 'Text2', 'TEXT3', 'tEXT4', 'Some other text', '1 more "Te_xt".'],
        ['text1', 'text2', 'text3', 'text4', 'some other text', '1 more "te_xt".'],
        [
          'format' => 'lower',
          'non_word' => '',
        ],
        'To lower case test',
      ],
      // To lower case no non-word.
      [
        ['text1', 'Text2', 'TEXT3', 'tEXT4', 'Some other text', '1 more "Te_xt".'],
        ['text1', 'text2', 'text3', 'text4', 'someothertext', '1moretext'],
        [
          'format' => 'lower',
          'non_word' => 'remove',
        ],
        'To lower case no non-word test',
      ],
      // To lower case underscores.
      [
        ['text1', 'Text2', 'TEXT3', 'tEXT4', 'Some other text', '1 more "Te_xt".'],
        ['text1', 'text2', 'text3', 'text4', 'some_other_text', '1_more_te_xt'],
        [
          'format' => 'lower',
          'non_word' => 'underscore',
        ],
        'To lower case underscores test',
      ],
      // Title case.
      [
        ['text1', 'Text2', 'TEXT3', 'tEXT4', 'Some other text', '1 more "Te_xt".'],
        ['Text1', 'Text2', 'Text3', 'Text4', 'Some Other Text', '1 More "Te_Xt".'],
        ['format' => 'title'],
        'Title test',
      ],
      // To lower camelCase case.
      [
        ['text1', 'Text2', 'TEXT3', 'tEXT4', 'Some other text', '1 more "Te_xt".'],
        ['text1', 'text2', 'text3', 'text4', 'someOtherText', '1MoreTeXt'],
        [
          'format' => 'lower_camel_case',
          'non_word' => '',
        ],
        'To lower camelCase test',
      ],
      // To upper camelCase case.
      [
        ['text1', 'Text2', 'TEXT3', 'tEXT4', 'Some other text', '1 more "Te_xt".'],
        ['Text1', 'Text2', 'Text3', 'Text4', 'SomeOtherText', '1MoreTeXt'],
        [
          'format' => 'upper_camel_case',
          'non_word' => '',
        ],
        'To upper camelCase test',
      ],
    ];
  }

  /**
   * Tests numeric unit data processor.
   *
   * @dataProvider provideTestStringCaseProcessor
   */
  public function testStringCaseProcessor(
    array $data,
    array $expected_result,
    array $config,
    string $test_name,
  ) {
    $string_translation = $this->getMockBuilder(TranslationInterface::class)
      ->getMock();

    $logger = $this->getMockBuilder(LoggerChannelInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $logger_factory = $this->getMockBuilder(LoggerChannelFactoryInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $logger_factory
      ->expects($this->any())
      ->method('get')
      ->with('xntt_data_processor_stringcase')
      ->willReturn($logger);

    $field_def = $this->getMockBuilder(FieldDefinitionInterface::class)
      ->getMock();

    $data_processor = new StringCase($config, 'stringcase', [], $string_translation, $logger_factory);

    $result = $data_processor->processData($data, $field_def, 'value');
    $this->assertEquals($expected_result, $result, $test_name);
  }

}
