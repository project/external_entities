External Entitites Views plugin
===============================

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

External Entities Views plugin enables the use of external entity data types in
views. This module is currently experimental and may not function as expected.
USE AT YOUR OWN RISKS.

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/external_entities

REQUIREMENTS
------------

This module requires the following modules:

 * Views
 * [External Entities](https://www.drupal.org/project/external_entities)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration.

MAINTAINERS
-----------

Current maintainers:
 * Valentin Guignon (guignonv) - https://www.drupal.org/u/guignonv
