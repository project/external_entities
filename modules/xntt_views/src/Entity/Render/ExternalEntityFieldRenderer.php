<?php

namespace Drupal\xntt_views\Entity\Render;

use Drupal\Core\Entity\EntityInterface;
use Drupal\views\Entity\Render\EntityFieldRenderer;
use Drupal\views\Plugin\views\PluginBase;
use Drupal\views\ResultRow;

/**
 * Renders entity fields.
 *
 * This is used to build render arrays for all entity field values of a view
 * result set sharing the same relationship. An entity translation renderer is
 * used internally to handle entity language properly.
 */
class ExternalEntityFieldRenderer extends EntityFieldRenderer {

  /**
   * {@inheritdoc}
   */
  public function getEntityTranslation(EntityInterface $entity, ResultRow $row) {
    // @todo Manage translated external entities.
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityTranslationRenderer() {
    if (!isset($this->entityTranslationRenderer)) {
      $view = $this->getView();
      $rendering_language = $view->display_handler
        ->getOption('rendering_language');
      $langcode = NULL;
      $dynamic_renderers = [
        '***LANGUAGE_entity_translation***' => 'TranslationLanguageRenderer',
        '***LANGUAGE_entity_default***' => 'DefaultLanguageRenderer',
      ];
      if (isset($dynamic_renderers[$rendering_language])) {
        // Dynamic language set based on result rows or instance defaults.
        $renderer = $dynamic_renderers[$rendering_language];
      }
      else {
        if (strpos($rendering_language, '***LANGUAGE_') !== FALSE) {
          $langcode = PluginBase::queryLanguageSubstitutions()[$rendering_language];
        }
        else {
          // Specific langcode set.
          $langcode = $rendering_language;
        }
        $renderer = 'ConfigurableLanguageRenderer';
      }
      // We do not support other language than default at the moment.
      // Other language rendered class are written to use SQL tables and
      // need to be replaced.
      // @todo Implement a way to manage language translations with xntt.
      // $class = '\\Drupal\\views\\Entity\\Render\\' . $renderer;
      $class = '\\Drupal\\views\\Entity\\Render\\DefaultLanguageRenderer';
      $entity_type = $this->getEntityTypeManager()
        ->getDefinition($this->getEntityTypeId());
      $this->entityTranslationRenderer = new $class($view, $this->getLanguageManager(), $entity_type, $langcode);
    }
    return $this->entityTranslationRenderer;
  }

}
