<?php

namespace Drupal\xntt_views\Plugin\views\query;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a Views query class for External Entities.
 *
 * To complete implementation, see ad example:
 * https://git.drupalcode.org/project/search_api/-/blob/8.x-1.x/src/Plugin/views/query/SearchApiQuery.php?ref_type=heads.
 *
 * @ViewsQuery(
 *   id = "xntt_views",
 *   title = @Translation("External Entities Query"),
 *   help = @Translation("The query will be generated and run on external entities.")
 * )
 */
class XnttViewsQuery extends QueryPluginBase {

  /**
   * The External Entity type ID.
   *
   * @var string
   */
  protected $xnttTypeId;

  /**
   * The external entity query object.
   *
   * @var \Drupal\external_entities\Entity\Query\External\Query
   */
  protected $query;

  /**
   * An array of sections of the WHERE query.
   *
   * Each section is in itself an array of pieces and a flag as to whether or
   * not it should be AND or OR.
   *
   * @var array
   */
  protected $where = [];

  /**
   * Array of "order by" clauses.
   *
   * @var array
   */
  protected array $orderby;

  /**
   * Associative array of field names.
   *
   * @var array
   */
  protected array $fields;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a \Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, ?array &$options = NULL) {
    parent::init($view, $display, $options);
    $views_data = Views::viewsData()->get($view->storage->get('base_table'));
    $this->xnttTypeId =
      $views_data['table']['base']['xntt']
      ?? preg_replace('/^xntt_views_/', '', $view->storage->get('base_table'));

    $xntt_loader = $this->entityTypeManager->getStorage($this->xnttTypeId);
    $this->query = $xntt_loader->getQuery()->accessCheck(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function ensureTable($table, $relationship = NULL) {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function addField($table, $field, $alias = '', $params = []) {
    $this->fields[$field] = [
      'field' => $field,
      'table' => '',
      'alias' => $field,
    ];
    return $field;
  }

  /**
   * {@inheritdoc}
   */
  public function addWhere($group, $field, $value = NULL, $operator = NULL) {
    // Ensure all variants of 0 are actually 0. Thus '', 0 and NULL are all
    // the default group.
    if (empty($group)) {
      $group = 0;
    }
    // Check for a group.
    if (!isset($this->where[$group])) {
      $this->setWhereGroup('AND', $group);
    }
    $this->where[$group]['conditions'][] = [
      'field' => ltrim($field, '.'),
      'value' => $value,
      'operator' => $operator,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function addOrderBy($table, $field = NULL, $order = 'ASC', $alias = '', $params = []) {
    $this->orderby[] = [
      'field' => $field ?? '',
      'direction' => strtoupper($order),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build(ViewExecutable $view) {
    $this->view = $view;
    // Initialize the pager and let it modify the query to add limits. This has
    // to be done even for aborted queries since it might otherwise lead to a
    // fatal error when Views tries to access $view->pager.
    $view->initPager();
    $view->pager->query();
  }

  /**
   * {@inheritdoc}
   */
  public function execute(ViewExecutable $view) {

    // Manage conditions.
    // @todo Manage complex conditions.
    if (isset($this->where)) {
      foreach ($this->where as $where_group => $where) {
        foreach ($where['conditions'] as $condition) {
          $this->query->condition(
            $condition['field'],
            $condition['value'],
            $condition['operator']
          );
        }
      }
    }

    $xntt_loader = $this->entityTypeManager->getStorage($this->xnttTypeId);
    $view->pager->preExecute($this->query);
    // Views passes sometimes NULL and sometimes the integer 0 for "All" in a
    // pager. If set to 0 items, a string "0" is passed. Therefore, we unset
    // the limit if an empty value OTHER than a string "0" was passed.
    if (!$this->limit && $this->limit !== '0') {
      $this->limit = NULL;
    }
    if (isset($this->offset)) {
      $this->query->range($this->offset, $this->limit);
    }

    foreach ($this->orderby ?? [] as $order) {
      $this->query->sort($order['field'], $order['direction']);
    }

    $start = microtime(TRUE);
    $count_query = clone $this->query;
    $xntts = $this->query->execute();
    $entities = $xntt_loader->loadMultiple(array_values($xntts));
    $view->execute_time = microtime(TRUE) - $start;

    $index = 0;
    foreach ($entities as $entity) {
      $row['index'] = $index++;
      $row['_entity'] = $entity;
      $fields = array_keys($this->fields ?? ['title' => 'title']);
      foreach ($fields as $field) {
        $row[$field] = $entity->get($field);
      }
      $view->result[] = new ResultRow($row);
    }

    $view->pager->total_items = $count_query->count()->execute();

    $view->pager->updatePageInfo();

    if (!empty($view->pager->options['offset'])) {
      $view->pager->total_items -= $view->pager->options['offset'];
    }
    $view->total_rows = $view->pager->total_items;

    // Fill info.
    // @todo Provide preview infos.
    // See /core/modules/views_ui/src/ViewUI.php (line 628...)
    $view->build_info = [
      // 'title' => 'External Entity Query',
      'query' => $this->query,
      'count_query' => $count_query,
      'query_args' => [],
    ]
    + $view->build_info;
  }

}
