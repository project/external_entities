<?php

namespace Drupal\xntt_views\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\StringFilter;

/**
 * Xntt_views string filter.
 *
 * Including default string filters plus fuzzy search (when available).
 *
 * @ViewsFilter("xntt_views_string")
 */
class XnttStringFilter extends StringFilter {

  /**
   * {@inheritdoc}
   */
  public function operators() {
    $operators = parent::operators();

    // Add fuzzy search.
    $operators += [
      'fuzzy' => [
        'title' => $this->t('Fuzzy search'),
        'short' => $this->t('fuzzy'),
        'method' => 'opFuzzy',
        'values' => 1,
      ],
    ];

    return $operators;
  }

  /**
   * Fuzzy search operator handler.
   *
   * @param string $field
   *   The name of the field to check.
   */
  protected function opFuzzy($field) {
    $operator = $this->getConditionOperator('FUZZY');
    $this->query->addWhere($this->options['group'], $field, $this->value, $operator);
  }

}
