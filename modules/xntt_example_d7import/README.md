# External entity type examples

This module demonstrates how to create programmatically an external entity
types that can be used to import remote Drupal 7 data into a local Drupal 9+
site through a Drupal 7 RESTful web service.

@todo Doc to complete (how to install on D7, how configure, how to customize
fecthed data, add fields, how xnttmanager could be used to complete import, ...)

## Drupal 7 RESTful Web Services

https://www.drupal.org/project/restws
