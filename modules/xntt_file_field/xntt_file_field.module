<?php

/**
 * @file
 * External Entities Files Storage Client module file.
 */

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\external_entities\Entity\ConfigurableExternalEntityTypeInterface;
use Drupal\external_entities\Entity\ExternalEntityInterface;
use Drupal\external_entities\ExternalEntityStorage;
use Drupal\link\Plugin\Field\FieldType\LinkItem;
use Drupal\xntt_file_field\Entity\ExternalFile;

/**
 * @file
 * External Entitites Files Storage Client plugin for External Entities.
 */

/**
 * Implements hook_entity_preload().
 *
 * This implementation enables the use of external entity identifiers prefixed
 * with "xntt-<content type machine name>-<uri drupal field machine name>-
 * <content identifier>#<file number>" as file identifiers in order to use those
 * external entities as Drupal managed files.
 * The matching external entities must return at least a non-empty 'uri' field
 * in order to be taken into account.
 */
function xntt_file_field_entity_preload(array $ids, string $entity_type_id) {
  $entities = [];
  if ('file' === $entity_type_id) {
    foreach ($ids as $id) {
      if (is_numeric($id)) {
        // Skip regular Drupal-managed files.
        continue;
      }

      $xntt_file_id_regex = '/^xntt-([a-z_]\w*)-([a-z_]\w*)-(.+?)(?:#(\d*))?$/';
      if (preg_match($xntt_file_id_regex, $id, $matches)) {
        try {
          [$m, $xntt_type, $file_field_name, $xntt_id] = $matches;
          $file_num = $matches[4] ?? 0;
          $storage =
            \Drupal::service('entity_type.manager')->getStorage($xntt_type);
          if (!is_a($storage, ExternalEntityStorage::class)) {
            continue;
          }
          $xntt = $storage->load($xntt_id);
          if (empty($xntt)) {
            continue;
          }
          try {
            $fm_config = $xntt->getExternalEntityType()->getFieldMapperConfig($file_field_name);
            if (empty($fm_config)) {
              throw new \InvalidArgumentException("No field mapper config found for field '$file_field_name' (external entity '$xntt_type')");
            }
            $uri_field_name = $fm_config['uri'];
            $field = $xntt->get($uri_field_name)->get($file_num);
            if ($field instanceof LinkItem) {
              $real_uri = $field->get('uri')->getValue();
            }
            else {
              $real_uri = $field->get('value')->getValue();
            }
          }
          catch (\InvalidArgumentException $e) {
            // Output a message to let know the mapping failed.
            \Drupal::logger('xnttfiles')->warning(
              'Failed to get mapped URI. '
              . $e
            );
          }
          // Make sure we got a URI.
          if (!empty($real_uri)) {
            // Filter URI if needed.
            if (!empty($fm_config['filter'])) {
              $match = preg_match(
                '#^'
                . $fm_config['filter']
                . '$#',
                $real_uri
              );
              if (FALSE === $match) {
                // Invalid regexp.
                \Drupal::logger('xnttfiles')->warning(
                  'Invalid regular expression for external file filtering (field "$file_field_name" of "$xntt_type"): '
                  . $fm_config['filter']
                );
                continue;
              }
              elseif (0 === $match) {
                // Did not pass filtering.
                \Drupal::logger('xnttfiles')->warning(
                  'External file URI did not pass filtering (field "$file_field_name" of "$xntt_type"): '
                  . $real_uri
                );
                continue;
              }
            }
            // Extract file name.
            if (preg_match('#([^\\/\?]+)(?:\?.*)?$#', $real_uri, $matches)) {
              $filename = $matches[1];
            }
            else {
              // Default file name.
              $filename = 'xnttfile';
            }
            // Append specified extension if one.
            if (!empty($fm_config['extension'])) {
              $filename .=
                '.'
                . $fm_config['extension'];
            }
            $file_data = [
              // Use the xntt stream URI.
              'uri' =>
              'xntt://'
              . $xntt_type
              . '/'
              . $file_field_name
              . '/'
              . $xntt_id
              . '/'
              . $file_num
              . '/'
              . $filename,
              'real_uri' => $real_uri,
              'filename' => $filename,
            ];
            ExternalFile::preCreate($storage, $file_data);
            $entities[$id] = ExternalFile::create($file_data);
            $entities[$id]->set('fid', $id);
            $entities[$id]->setPermanent();
          }
        }
        catch (PluginNotFoundException $e) {
          // Just ignore.
        }
      }
      // @todo what to do with unmatched invalid file ids? It may cause issue
      // on DB queries later. It should be checked for both Postgres and MySQL.
    }
  }
  return $entities;
}

/**
 * Implements hook_entity_storage_load().
 *
 * Only update external entity file or image fields that have been mapped.
 * Sets the correct external file virtual identifier so it could be loaded later
 * on when the system will try to access it through "xntt://" stream.
 */
function xntt_file_field_entity_storage_load(array $entities, $entity_type_id) {
  foreach ($entities as $entity) {

    // Only work on external entities.
    if (!$entity instanceof ExternalEntityInterface) {
      continue;
    }

    // Only work with configurable external entities types.
    $xntt = $entity->getExternalEntityType();
    if (!$xntt instanceof ConfigurableExternalEntityTypeInterface) {
      continue;
    }

    // Check if we got a file mapping.
    // Get all field types.
    $file_fields = array_filter(
      $xntt->getMappableFields(),
      function ($field_definition) {
        return in_array($field_definition->getType(), ['file', 'image']);
      }
    );

    foreach ($file_fields as $field_name => $field_definition) {
      $fm_config = $xntt->getFieldMapperConfig($field_name);
      if (!empty($fm_config['uri'])) {
        // Got xnttfile mapping, apply it.
        $type_id = $xntt->getDerivedEntityType()->id();
        // Set file field mapping ot a special file ID using the format:
        // Format: "xntt-xntt_type-file_field_name-xntt_instance_id#delta"
        // We got the delta from the number of values in field $mapping['uri']
        // which holds the "real" file URI values.
        $values = [];
        try {
          $value_count = count($entity->get($fm_config['uri']));
        }
        catch (\InvalidArgumentException $e) {
          // Field does not seem to exist, skip.
          continue;
        }
        for ($i = 0; $i < $value_count; ++$i) {
          $values[] =
            'xntt-'
            . $type_id
            . '-'
            . $field_name
            . '-'
            . $entity->id()
            . '#'
            . $i;
        }
        $entity->$field_name = $values;
      }
    }
  }
}

/**
 * Implements hook_config_schema_info_alter().
 */
function xntt_file_field_config_schema_info_alter(&$definitions) {
  // Add config elements to schema.
  $fm_base_key = 'plugin.plugin_configuration.external_entities_field_mapper.';
  foreach ($definitions as $key => &$definition) {
    if (0 === strncmp($key, $fm_base_key, strlen($fm_base_key))) {
      $definition['mapping']['xnttfiles'] = [
        'type' => 'sequence',
        'label' => 'File field mappings',
        'sequence' => [
          'type' => 'mapping',
          'mapping' => [
            'fid' => [
              'type' => 'machine_name',
              'label' => 'Internal identifier',
            ],
            'uri' => [
              'type' => 'machine_name',
              'label' => 'Machine name of the field holding the file URI',
            ],
            'filter' => [
              'type' => 'string',
              'label' => 'URI validation filter',
            ],
            'extension' => [
              'type' => 'string',
              'label' => 'Virtual file extension to use',
            ],
          ],
        ],
      ];
    }
  }
}
