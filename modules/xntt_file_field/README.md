External Entities File and Image Fields Support
***********************************************

This module enables the use of Drupal file and image fields with external entity
types using file path or URL as file source.

===============================

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * How it works
 * Security
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

Drupal file and image fields are only working with file objects stored in Drupal
database. Those fields use a database file identifier rather than a path or a
URL. Therefore, since external entity data is not stored into Drupal database,
those fields can't be used natively with external entities.

To get around this problem, a first solution was to use the
[ImageCache External](https://www.drupal.org/project/imagecache_external) module
for image URLs provided by remote content. While it allowed to display images,
that module relies on the link field type and it was not possible to use other
module features that work on file and image fields (image styles, some views
options, Flex Slider, etc.).

This module provides a way to use file path or URL provided by an external
entity source as a file or image field. To do so, you will need to add an extra
text field (even not displayed) to your external entity that will be mapped to
the target file path or URL. Then, you will be able to map your file or image
field to the path mapped to that field using the "File field mapping" section
added on the external entity field mapping page (after the "Configure *** field
mapper" section).

Since some file URL may not contain the file extension while the file extension
is required by Drupal to determine the file (mime) type, it is possible to
"force" the file extension and append virtually that extension to the remote
file so Drupal can validate the file type and use it.

HOW IT WORKS
-------------

Since Drupal file and image fields are mapped to managed file identifiers, this
module uses and intercepts special file field identifiers (prefixed by "xntt-")
to manage external files. Those special identifiers follow the form
"xntt-xntt_type-file_field_name-xntt_instance_id#delta" are used to generate
a file path using the stream wrapper "xntt:". From that path, the stream wrapper
is able to retrieve the corresponding external entity instance that holds the
field containing the file path or URL and then serve that file.

SECURITY
------------

Since it is possible to map any local file, the interface allows to use a
regular expression pattern to filter provided external file path and only allow
those passing the filter. Therefore, it is up to the person who defines the
mapping through the external entity type definition to limit access to whatever
part of the local file system or remote URLs.

Furthermore, this module does not allow any file alteration but only read
access.

REQUIREMENTS
------------

This module requires the following modules:

 * [External Entities](https://www.drupal.org/project/external_entities)

INSTALLATION
------------

 * This module is a sub-module of the External Entities module. Just enable it
   either through the "Extend" admin UI or using drush.

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will just add a "File field mapping" section to external
entity types "Field mapping" page after the "Configure *** field mapper"
section IF the external entity type uses at least one file or image field.

MAINTAINERS
-----------

Current maintainers:
 * Valentin Guignon (guignonv) - https://www.drupal.org/u/guignonv
