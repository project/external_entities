<?php

namespace Drupal\xntt_file_field\Plugin\ExternalEntities\FieldMapper;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\external_entities\Entity\ExternalEntityInterface;
use Drupal\external_entities\FieldMapper\FieldMapperBase;

/**
 * This is a short description of the function.
 *
 * This is additional information about the function.
 * It should be on a separate line from the short description.
 *
 * @param int $parameter Description of the parameter.
 *
 * @return string Description of the return value.
 *
 * @FieldMapper(
 *   id = "file",
 *   label = @Translation("File field mapper"),
 *   description = @Translation("Provides an interface to map file and image fields."),
 *   field_types = {
 *     "file",
 *     "image"
 *   }
 * )
 *
 * @package Drupal\external_entities\Plugin\ExternalEntities\FieldMapper
 */
class FileFieldMapper extends FieldMapperBase {

  /**
   * {@inheritdoc}
   */
  protected function initProperties() {
    parent::initProperties();
    $this->properties[static::SPECIFIC_PROPERTIES]['target_id'] =
      $this->properties[static::GENERAL_PROPERTIES]['target_id'];
    unset($this->properties[static::GENERAL_PROPERTIES]['target_id']);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['uri' => '']
      + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state,
  ) {
    $xntt_type = $this->externalEntityType;
    $derived_entity_type = $xntt_type->getDerivedEntityType();

    $fields = [];
    if (!empty($derived_entity_type)) {
      $fields = $this->entityFieldManager->getFieldDefinitions(
        $derived_entity_type->id(),
        $derived_entity_type->id()
      );
      $file_fields = array_filter(
        $fields,
        function (FieldDefinitionInterface $field) {
          return $field->getName() !== ExternalEntityInterface::ANNOTATION_FIELD
            && !$field->isComputed()
            && (('file' == $field->getType())
              || ('image' == $field->getType()));
        }
      );
    }

    $fm_config = $this->getConfiguration();
    $text_field_options = [];
    $integer_field_options = [];
    $date_field_options = [];
    $lang_field_options = [];
    $uri_field_options = [];
    $ref_field_options = [];
    $uuid_field_options = [];
    foreach ($fields as $field_name => $field_definition) {
      // Get list of considered field by types.
      switch ($field_definition->getType()) {
        case 'string':
        case 'string_long':
        case 'list_string':
          $text_field_options[$field_name] = $field_definition->getLabel();
          break;

        case 'uri':
        case 'link':
          $uri_field_options[$field_name] = $field_definition->getLabel();
          break;

        case 'entity_reference':
          $ref_field_options[$field_name] = $field_definition->getLabel();
          break;

        default:
      }
    }

    $title = $field_definition->getLabel();
    $form['fid'] = [
      '#type' => 'hidden',
      // Will be set later if needed.
      '#value' => '',
    ];
    $form['uri'] = [
      '#type' => 'select',
      '#title' => $this->t('File URI (URL/path) field'),
      '#required' => TRUE,
      '#description' => $this->t(
        'To map a file field, the file URI must be mapped to a plain text/link field (of current entity), even if that field is not displayed but it could be used to edit file URIs.'
      ),
      '#options' => $text_field_options + $uri_field_options,
      '#empty_value' => '',
      '#default_value' => $fm_config['uri']
      ?? NULL,
    ];
    $form['filter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Regular expression to filter allowed URI (optional)'),
      '#required' => FALSE,
      '#default_value' => $fm_config['filter']
      ?? NULL,
      '#description' => $this->t(
        'Leave empty to disable filtering. Do not include regex delimiters (the given regexp will be processed between "#^" and "$#"). Ex.: to allow only text files: ".*\.txt" or a sub-path: "/shared/drupal/.*"'
      ),
    ];
    $form['extension'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Force file extension (optional)'),
      '#required' => FALSE,
      '#default_value' => $fm_config['extension']
      ?? NULL,
      '#description' => $this->t(
        'Do not include dot prefix. Specifying an extension might be required to guess file type if the given URI does not provide a full file name.'
      ),
      '#field_prefix' => '<i>filename.</i>',
    ];
    // @todo Add possibility to use another entity source to fill file fields.
    $form = parent::buildConfigurationForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // @todo Validate uri, filter and extension values.
    $filter = $form_state->getValue('filter');
    if (!empty($filter)) {
      if (FALSE === preg_match('#^' . $filter . '$#', 'file.ext')) {
        // Regex failed.
        $form_state->setErrorByName(
          'filter',
          $this->t(
            'The regular expression to filter allowed URI failed to compile with the error: @error',
            ['@error' => preg_last_error_msg()]
          )
        );
      }
    }
    $extension = $form_state->getValue('extension');
    if (!empty($extension)) {
      $extension = preg_replace('/^[\s\.]*/', '', $extension);
      if (!preg_match('#^\w+$#', $extension)) {
        // Invalid extension.
        $form_state->setErrorByName(
          'extension',
          $this->t(
            'The optional extension field contains invalid characters (ie. non-word characters).'
          )
        );
      }
      $form_state->setValue('extension', $extension);
    }
    parent::validateConfigurationForm($form, $form_state);
  }

}
