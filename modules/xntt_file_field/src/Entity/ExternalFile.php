<?php

namespace Drupal\xntt_file_field\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\Entity\File as DrupalFile;

/**
 * Defines the external entity file entity class.
 *
 * This class is used to expose some external entities with a 'uri' file as a
 * Drupal managed file in order to use those in file field mapping. However,
 * the mapping must not be set to the external entity identifier directly but
 * rather to that identifier with a prefix 'xnttfiles-' followed by the external
 * entity type name followed by a dash.
 *
 * @see xntt_file_field_entity_preload()
 *
 * @ContentEntityType(
 *   id = "xnttfile",
 *   label = @Translation("External file"),
 *   label_collection = @Translation("External files"),
 *   label_singular = @Translation("external file"),
 *   label_plural = @Translation("external files"),
 *   label_count = @PluralTranslation(
 *     singular = "@count external file",
 *     plural = "@count external files",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\file\FileStorage",
 *     "storage_schema" = "Drupal\file\FileStorageSchema",
 *     "access" = "Drupal\file\FileAccessControlHandler",
 *     "views_data" = "Drupal\file\FileViewsData",
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *     "form" = {
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\file\Entity\FileRouteProvider",
 *     },
 *   },
 *   base_table = "file_managed",
 *   entity_keys = {
 *     "id" = "fid",
 *     "label" = "filename",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "delete-form" = "/file/{file}/delete",
 *   }
 * )
 */
class ExternalFile extends DrupalFile {

  /**
   * {@inheritdoc}
   *
   * Does not save external files.
   */
  public function save() {
    // Do nothing.
    return 0;
  }

  /**
   * {@inheritdoc}
   *
   * Does not save external files.
   */
  public function preSave(EntityStorageInterface $storage) {
    // Do nothing.
  }

  /**
   * {@inheritdoc}
   *
   * Does not remove external files.
   */
  public function delete() {
    // Do nothing.
  }

  /**
   * {@inheritdoc}
   *
   * Does not remove external files.
   */
  public static function preDelete(
    EntityStorageInterface $storage,
    array $entities,
  ) {
    // Do nothing.
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(
    EntityTypeInterface $entity_type,
  ) {
    $fields = parent::baseFieldDefinitions($entity_type);
    // Note: changing the fid type here has no real effect.
    $fields['fid'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('ID'))
      ->setReadOnly(TRUE)
      ->setLabel(t('File ID'))
      ->setDescription(t('The file ID.'));
    return $fields;
  }

}
