<?php

namespace Drupal\xnttsql\Exception;

/**
 * Exception thrown by External Entities SQL Database Storage Client.
 */
class DatabaseExternalEntityException extends \RuntimeException {}
