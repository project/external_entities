<?php

namespace Drupal\external_entities\Plugin\ExternalEntities\DataAggregator;

use Drupal\Core\Form\FormStateInterface;

/**
 * External entities data aggregator by groups.
 *
 * @DataAggregator(
 *   id = "horizontal",
 *   label = @Translation("Horizontal data aggregator"),
 *   description = @Translation("Cumulates entities from multiple data sources.")
 * )
 */
class HorizontalDataAggregator extends GroupAggregator {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['multi_info'] = [
      '#type' => 'item',
      '#markup' => $this->t(
        'This data aggregator cumulates entities from multiple data sources into a global set.'
      ),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildStorageClientAggregationForm(
    array $form,
    FormStateInterface $form_state,
    int $client_index,
  ) :array {
    $aggr_selector = ($form['#attributes']['id'] ??= uniqid('aggr', TRUE));
    $aggr_config = $this->getConfiguration()['storage_clients'] ?? [];
    // Data aggregation settings.
    $storage_client_form = [
      'groups' => [
        '#type' => 'textfield',
        '#title' => $this->t('Idientifier prefix'),
        '#description' => $this->t('This prefix must be unique and will be used in external entity identifiers to match the source storage client to use. While the prefix will be visible from the Drupal side, it might be automatically removed when querying the storage source if it is set to "virtual" (see below).'),
        '#default_value' => implode(';', $aggr_config[$client_index]['aggr']['groups'] ?? []),
        '#required' => TRUE,
        '#attributes' => [
          'id' => $aggr_selector . '_groups',
        ],
      ],
      'group_prefix_strip' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Virtual prefix'),
        '#description' => $this->t('"Virtual prefix" means the prefix is not present on the storage source side but only visible from the Drupal side.'),
        '#default_value' => $aggr_config[$client_index]['aggr']['group_prefix_strip'] ?? FALSE,
      ],
    ];

    // We simplify UI and let R/W management be at the global external entity
    // level.
    $storage_client_form['mode'] = [
      '#type' => 'hidden',
      '#default_value' => static::STORAGE_CLIENT_MODE_READWRITE,
    ];

    return $storage_client_form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateStorageClientAggregationForm(
    array $form,
    FormStateInterface $form_state,
    int $client_index,
    array &$group_prefixes = [],
  ) {
    // We still allow multiple prefixes while it is not displayed for clarity.
    $aggr_settings = $form_state->getValue(['storage_clients', $client_index, 'aggr'], []);
    $aggr_settings['groups'] = array_filter(
      preg_split('/\s*;\s*/', trim($aggr_settings['groups'] ?? '')),
      'strlen'
    );
    $aggr_settings['id_field'] = '';
    $aggr_settings['merge'] = 'over';
    $aggr_settings['merge_as_member'] = '';
    $aggr_settings['merge_join'] = '';
    $form_state->setValue(['storage_clients', $client_index, 'aggr'], $aggr_settings);

    // Check prefixes...
    // Prefixes must appear only once.
    foreach ($aggr_settings['groups'] as $prefix) {
      if (!empty($prefix) && array_key_exists($prefix, $group_prefixes)) {
        $form_state->setError(
          $form['storage_clients'][$client_index]['aggr']['groups'],
          $this->t(
            'The prefix %prefix appears more than once. In horinzontal data aggregation, each storage client prefix must be unique.',
            [
              '%prefix' => $prefix,
            ]
          )
        );
      }
      $group_prefixes[$prefix] = $prefix;
    }

    // Make sure no prefix is part of another one.
    foreach ($aggr_settings['groups'] as $group_prefix1) {
      if ('' == $group_prefix1) {
        continue 1;
      }
      foreach ($group_prefixes as $group_prefix2) {
        if ('' == $group_prefix2) {
          continue 1;
        }
        if (($group_prefix1 != $group_prefix2)) {
          if (strncmp($group_prefix2, $group_prefix1, strlen($group_prefix1)) === 0) {
            $form_state->setError(
              $form['storage_clients'][$client_index]['aggr']['groups'],
              $this->t(
                'In external entity storage client "Multiple storages", the prefix %prefix1 is also a prefix for prefix %prefix2. Prefixes must be strictly distinct.',
                [
                  '%prefix1' => $group_prefix1,
                  '%prefix2' => $group_prefix2,
                ]
              )
            );
          }
          elseif (strncmp($group_prefix1, $group_prefix2, strlen($group_prefix2)) === 0) {
            $form_state->setError(
              $form['storage_clients'][$client_index]['aggr']['groups'],
              $this->t(
                'In external entity storage client "Multiple storages", the prefix %prefix1 is also a prefix for prefix %prefix2. Prefixes must be strictly distinct.',
                [
                  '%prefix1' => $group_prefix2,
                  '%prefix2' => $group_prefix1,
                ]
              )
            );
          }
        }
      }
    }
  }

}
