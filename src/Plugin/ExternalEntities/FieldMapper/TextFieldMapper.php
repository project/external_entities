<?php

namespace Drupal\external_entities\Plugin\ExternalEntities\FieldMapper;

use Drupal\Core\Form\FormStateInterface;
use Drupal\external_entities\FieldMapper\FieldMapperBase;

/**
 * This is a short description of the function.
 *
 * This is additional information about the function.
 * It should be on a separate line from the short description.
 *
 * @param int $parameter Description of the parameter.
 *
 * @return string Description of the return value.
 *
 * @FieldMapper(
 *   id = "text",
 *   label = @Translation("Text field mapper"),
 *   description = @Translation("Provides an interface to map formatted text fields."),
 *   field_types = {
 *     "text",
 *     "text_long",
 *     "text_with_summary"
 *   }
 * )
 *
 * @package Drupal\external_entities\Plugin\ExternalEntities\FieldMapper
 */
class TextFieldMapper extends FieldMapperBase {

  /**
   * {@inheritdoc}
   */
  protected function initProperties() {
    parent::initProperties();
    $this->properties[static::SPECIFIC_PROPERTIES]['value'] =
      $this->properties[static::GENERAL_PROPERTIES]['value'];
    $this->properties[static::SPECIFIC_PROPERTIES]['format'] =
      $this->properties[static::GENERAL_PROPERTIES]['format'];
    unset($this->properties[static::GENERAL_PROPERTIES]['value']);
    unset($this->properties[static::GENERAL_PROPERTIES]['format']);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['format' => 'plain']
      + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state,
  ) {

    $specific_properties = $this->getProperties(static::SPECIFIC_PROPERTIES);
    $this->buildPropertyMapperSelectForm($form, $form_state, 'value', $specific_properties['value']);
    $this->buildPropertyMapperConfigForm($form, $form_state, 'value', $specific_properties['value']);
    $this->buildTextFormatSelectionForm($form, $form_state);

    $form = parent::buildConfigurationForm($form, $form_state);

    return $form;
  }

  /**
   * Build a form element for configuring a field property mapping.
   *
   * @todo API: Return $form array instead of using reference &$form.
   *
   * @param array &$form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function buildTextFormatSelectionForm(
    array &$form,
    FormStateInterface $form_state,
  ) {
    $configuration = $this->getConfiguration();
    $format_options = [];
    $formats = filter_formats();
    foreach ($formats as $format_id => $format) {
      $format_options[$format_id] = $format->label();
    }

    $allowed_formats = array_flip(
      $this->getFieldDefinition()->getSettings()["allowed_formats"]
      ?? []
    );
    if (!empty($allowed_formats)) {
      // Restrict to allowed filter formats.
      $format_options = array_filter(
        $format_options,
        function ($format) use ($allowed_formats) {
          return array_key_exists($format, $allowed_formats);
        },
        ARRAY_FILTER_USE_KEY
      );
    }

    $form['format'] = [
      '#type' => 'select',
      '#title' => $this->t('Text format'),
      '#options' => $format_options,
      '#default_value' => $configuration['format'] ?? NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    // Validate value.
    $specific_properties = $this->getProperties(static::SPECIFIC_PROPERTIES);
    $this->validatePropertyForms(
      $form,
      $form_state,
      ['value' => $specific_properties['value']]
    );
    // Validate format.
    // @todo Check if format is one of the available ones for this field.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Submit and save text and format property mapper configs.
    $format = $form_state->getValue('format');
    $form_state->setValue('format', $format);
    $specific_properties = $this->getProperties(static::SPECIFIC_PROPERTIES);
    $this->submitPropertyForms(
      $form,
      $form_state,
      ['value' => $specific_properties['value']]
    );
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function extractFieldValuesFromRawData(
    array $raw_data,
    array &$context = [],
  ) :?array {
    $format = $this->getConfiguration()['format'] ?? '';
    $field_array = parent::extractFieldValuesFromRawData($raw_data, $context);
    foreach ($field_array as $delta => $field_value) {
      $field_array[$delta]['format'] = $format;
    }
    return $field_array;
  }

}
