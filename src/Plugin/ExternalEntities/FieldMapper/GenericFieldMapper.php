<?php

namespace Drupal\external_entities\Plugin\ExternalEntities\FieldMapper;

use Drupal\external_entities\FieldMapper\FieldMapperBase;

/**
 * This is a short description of the function.
 *
 * This is additional information about the function.
 * It should be on a separate line from the short description.
 *
 * @param int $parameter Description of the parameter.
 *
 * @return string Description of the return value.
 *
 * @FieldMapper(
 *   id = "generic",
 *   label = @Translation("Generic"),
 *   description = @Translation("Provides a generic interface to map any type of field."),
 *   field_types ={
 *     "*"
 *   }
 * )
 *
 * @package Drupal\external_entities\Plugin\ExternalEntities\FieldMapper
 */
class GenericFieldMapper extends FieldMapperBase {

}
