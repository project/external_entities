<?php

namespace Drupal\external_entities\Plugin\ExternalEntities\DataProcessor;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\external_entities\DataProcessor\DataProcessorBase;

/**
 * This plugin handles string character case alterations.
 *
 * @DataProcessor(
 *   id = "stringcase",
 *   label = @Translation("String case"),
 *   description = @Translation("Change character case.")
 * )
 *
 * @package Drupal\external_entities\Plugin\ExternalEntities\DataProcessor
 */
class StringCase extends DataProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'format' => '',
      'non_word' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state,
  ) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $config = $this->getConfiguration();
    $form['format'] = [
      '#type' => 'radios',
      '#title' => $this->t('Case altering'),
      '#options' => [
        '' => $this->t('Unchanged'),
        'upper' => $this->t('Full upper-case'),
        'lower' => $this->t('Full lower-case'),
        'title' => $this->t('Full title-case conversion (upper-case first letter of all words)'),
        'lower_camel_case' => $this->t('To lower camelCase'),
        'upper_camel_case' => $this->t('To upper CamelCase'),
      ],
      '#default_value' => $config['format'] ?? '',
    ];

    $form['non_word'] = [
      '#type' => 'radios',
      '#title' => $this->t('Non-word character handling'),
      '#options' => [
        '' => $this->t('Unchanged'),
        'remove' => $this->t('Remove'),
        'underscore' => $this->t('Replace by underscores'),
      ],
      '#default_value' => $config['non_word'] ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function processData(
    array $raw_data,
    FieldDefinitionInterface $field_definition,
    string $property_name,
  ) :array {
    $config = $this->getConfiguration();
    $data = [];
    foreach ($raw_data as $entry) {
      if (!isset($entry)) {
        $data[] = NULL;
        continue;
      }
      switch ($config['format'] ?? '') {
        case 'upper':
          $data[] = mb_convert_case($entry, MB_CASE_UPPER);
          break;

        case 'lower':
          $data[] = mb_convert_case($entry, MB_CASE_LOWER);
          break;

        case 'title':
          $data[] = mb_convert_case($entry, MB_CASE_TITLE);
          break;

        case 'lower_camel_case':
          $camel = preg_replace(
            '#[\W_]+#',
            '',
            mb_convert_case($entry, MB_CASE_TITLE)
          );
          if (!empty($camel)) {
            $camel[0] = mb_convert_case($camel[0], MB_CASE_LOWER);
          }
          $data[] = $camel;
          break;

        case 'upper_camel_case':
          $data[] = preg_replace(
            '#[\W_]+#',
            '',
            mb_convert_case($entry, MB_CASE_TITLE)
          );
          break;

        default:
          $data[] = $entry;
          break;
      }
    }
    // Check non-word character handling.
    if (!empty($config['non_word'])) {
      switch ($config['non_word'] ?? '') {
        case 'remove':
          $data = array_map(
            function ($entry) {
              if (!isset($entry)) {
                return NULL;
              }
              return preg_replace('#[\W_]+#', '', $entry);
            },
            $data
          );
          break;

        case 'underscore':
          $data = array_map(
            function ($entry) {
              if (!isset($entry)) {
                return NULL;
              }
              return trim(preg_replace('#[\W_]+#', '_', $entry), '_');
            },
            $data
          );
          break;

        default:
          break;
      }
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function reverseDataProcessing(
    array $data,
    array $original_data,
    FieldDefinitionInterface $field_definition,
    string $property_name,
  ) :array|null {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function couldReverseDataProcessing() :bool {
    return FALSE;
  }

}
