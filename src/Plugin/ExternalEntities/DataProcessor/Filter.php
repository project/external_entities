<?php

namespace Drupal\external_entities\Plugin\ExternalEntities\DataProcessor;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\external_entities\DataProcessor\DataProcessorBase;

/**
 * This plugin handles filtering.
 *
 * @DataProcessor(
 *   id = "filter",
 *   label = @Translation("Value filtering"),
 *   description = @Translation("Removes values not passing the filter.")
 * )
 *
 * @package Drupal\external_entities\Plugin\ExternalEntities\DataProcessor
 */
class Filter extends DataProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'filtered' => [],
      'filter_empty' => 'null',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state,
  ) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['filter_empty'] = [
      '#type' => 'radios',
      '#options' => [
        'null' => $this->t('Remove null elements'),
        'empty' => $this->t('Remove empty elements'),
        'zerook' => $this->t('Remove empty elements but keep "0" values'),
        '' => $this->t('Only use filters below'),
      ],
      '#default_value' => $config['filter_empty'] ?? 'null',
    ];

    $values = implode(
      "\n",
      array_keys($config['filtered'] ?? [])
    );
    $form['filters'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Filters'),
      '#description' => $this->t('Enter a list of source values that should be filtered out (removed), one by line. You can leave this field empty to only use the behavior selected above.'),
      '#rows' => 5,
      '#default_value' => $values,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $form_state,
  ) {
    $values = array_filter(
      explode("\n", $form_state->getValue('filters', '')),
      'strlen'
    );
    $form_state->setValue('filtered', array_fill_keys($values, 'remove'));
    $form_state->unsetValue('filters');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function processData(
    array $raw_data,
    FieldDefinitionInterface $field_definition,
    string $property_name,
  ) :array {
    $config = $this->getConfiguration();
    $data = [];
    foreach ($raw_data as $entry) {
      if (!isset($entry)) {
        // Only keep NULL values if needed.
        if ('' == $config['filter_empty']) {
          $data[] = NULL;
        }
      }
      else {
        if (!array_key_exists($entry, $config['filtered'])
          && !(('empty' == $config['filter_empty']) && empty($entry))
          && !(('zerook' == $config['filter_empty']) && empty($entry) && ('0' != $entry))
        ) {
          $data[] = $entry;
        }
      }
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function reverseDataProcessing(
    array $data,
    array $original_data,
    FieldDefinitionInterface $field_definition,
    string $property_name,
  ) :array|null {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function couldReverseDataProcessing() :bool {
    // We can not reverse values as we do not know which new value correspond to
    // which original source value since we changed the order by potentially
    // removing some values.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function mayAlterData() :bool {
    // No data alteration, just removing some values where requested.
    return FALSE;
  }

}
