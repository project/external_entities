<?php

namespace Drupal\external_entities\Plugin\ExternalEntities\DataProcessor;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\TypedData\Plugin\DataType\DateTimeIso8601;
use Drupal\Core\TypedData\PrimitiveInterface;
use Drupal\Core\TypedData\Type\DateTimeInterface;
use Drupal\Core\TypedData\TypedDataManagerInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\external_entities\DataProcessor\DataProcessorBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This plugin handles field property data type auto-detection for type cast.
 *
 * @DataProcessor(
 *   id = "default",
 *   label = @Translation("Autodetect datatype"),
 *   description = @Translation("Data processor using Drupal field definition to detect datatype to use.")
 * )
 *
 * @package Drupal\external_entities\Plugin\ExternalEntities\DataProcessor
 */
class DefaultProcessor extends DataProcessorBase {

  /**
   * The typed data manager.
   *
   * @var \Drupal\Core\TypedData\TypedDataManagerInterface
   */
  protected $typedDataManager;

  /**
   * Constructs a DefaultProcessor object.
   *
   * The configuration parameters is expected to contain the external entity
   * type (key ExternalEntityTypeInterface::XNTT_TYPE_PROP), the field name
   * (key 'field_name') and the property name (key 'property_name') this data
   * processor will work on.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The identifier for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   * @param \Drupal\Core\TypedData\TypedDataManagerInterface $typed_data_manager
   *   The typed data manager.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    TranslationInterface $string_translation,
    LoggerChannelFactoryInterface $logger_factory,
    TypedDataManagerInterface $typed_data_manager,
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $string_translation,
      $logger_factory
    );
    $this->typedDataManager = $typed_data_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('string_translation'),
      $container->get('logger.factory'),
      $container->get('typed_data_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function processData(
    array $raw_data,
    FieldDefinitionInterface $field_definition,
    string $property_name,
  ) :array {
    $data = [];

    // Create the typed data instance.
    $property_definition = $field_definition
      ->getFieldStorageDefinition()
      ->getPropertyDefinition($property_name);
    $typed_data = $this->typedDataManager->create($property_definition);
    foreach ($raw_data as $entry) {

      if (!isset($entry)) {
        $data[] = NULL;
        continue;
      }

      // Provide rudimentary support for datetime-based fields by making sure
      // they are in the format as expected by Drupal.
      if (in_array(DateTimeInterface::class, class_implements($typed_data, TRUE))) {
        $timestamp = $entry !== NULL && !is_numeric($entry)
          ? strtotime($entry)
          : $entry;

        if (is_numeric($timestamp)) {
          if ($typed_data instanceof DateTimeIso8601) {
            $datetime_type = $field_definition
              ->getFieldStorageDefinition()
              ->getSetting('datetime_type');

            if ($datetime_type === DateTimeItem::DATETIME_TYPE_DATE) {
              $storage_format = DateTimeItemInterface::DATE_STORAGE_FORMAT;
            }
            else {
              $storage_format = DateTimeItemInterface::DATETIME_STORAGE_FORMAT;
            }

            // Use setValue so timezone is not set.
            $typed_data->setValue(gmdate($storage_format, $timestamp));
          }
          else {
            $typed_data->setDateTime(
              DrupalDateTime::createFromTimestamp($timestamp)
            );
          }
        }
      }
      else {
        $typed_data->setValue($entry);
      }

      // Convert the property value to the correct PHP type as expected by this
      // specific property type.
      if ($typed_data instanceof PrimitiveInterface) {
        $entry = $typed_data->getCastedValue();
      }

      $data[] = $entry;
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function reverseDataProcessing(
    array $data,
    array $original_data,
    FieldDefinitionInterface $field_definition,
    string $property_name,
  ) :array|null {
    // @todo Implement...
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function couldReverseDataProcessing() :bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function mayAlterData() :bool {
    // No data alteration, just a type cast.
    return FALSE;
  }

}
