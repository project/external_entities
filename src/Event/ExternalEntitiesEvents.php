<?php

namespace Drupal\external_entities\Event;

/**
 * Defines events for the external entities module.
 *
 * @see \Drupal\Core\Config\ConfigCrudEvent
 */
final class ExternalEntitiesEvents {

  /**
   * Name of the event fired before mapping raw data to an external entity.
   *
   * This event allows you to perform alterations on the raw data before
   * mapping.
   *
   * @Event
   */
  const TRANSFORM_RAW_DATA = 'external_entity.transform_raw_data';

  /**
   * Name of the event fired when extracting raw data from an external entity.
   *
   * This event allows you to perform alterations on the raw data after
   * extraction.
   *
   * @Event
   */
  const EXTRACT_RAW_DATA = 'external_entity.extract_raw_data';

  /**
   * Name of the event fired when mapping raw data to an external entity.
   *
   * This event allows you to perform alterations on the external entity after
   * mapping.
   *
   * @Event
   */
  const MAP_RAW_DATA = 'external_entity.map_raw_data';

  /**
   * Name of the event fired when retrieving mappable fields.
   *
   * This event allows you to perform alterations on the list of mappable
   * fields.
   *
   * @Event
   */
  const GET_MAPPABLE_FIELDS = 'external_entity.get_mappable_fields';

  /**
   * Name of the event fired when transliterating Drupal filters.
   *
   * This event allows you to perform alterations on the transliterated filters
   * returned by storage clients when calling ::transliterateDrupalFilters().
   *
   * @Event
   */
  const TRANSLITERATE_DRUPAL_FILTERS = 'external_entity.transliterate_drupal_filters';

  /**
   * Name of the event fired when transliterating Drupal sorts.
   *
   * This event allows you to perform alterations on the transliterated sorts
   * returned by storage clients when calling ::transliterateDrupalSorts().
   *
   * @Event
   */
  const TRANSLITERATE_DRUPAL_SORTS = 'external_entity.transliterate_drupal_sorts';

}
