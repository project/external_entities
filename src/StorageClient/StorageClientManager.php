<?php

namespace Drupal\external_entities\StorageClient;

use Drupal\Component\Plugin\FallbackPluginManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\external_entities\DataAggregator\DataAggregatorBase;

/**
 * StorageClient plugin manager.
 */
class StorageClientManager extends DefaultPluginManager implements FallbackPluginManagerInterface {

  /**
   * Constructs an StorageClientManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
  ) {
    parent::__construct(
      'Plugin/ExternalEntities/StorageClient',
      $namespaces,
      $module_handler,
      'Drupal\external_entities\StorageClient\StorageClientInterface',
      'Drupal\external_entities\Annotation\StorageClient'
    );
    $this->alterInfo('external_entities_storage_client_info');
    $this->setCacheBackend($cache_backend, 'external_entities_storage_client', ['external_entities_storage_client']);
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackPluginId($plugin_id, array $configuration = []) {
    \Drupal::messenger()->addWarning(
      t(
        "WARNING: Failed to load storage client plugin '@plugin'. Does the plugin exists and is enabled? Is the plugin cache up-to-date? It has been replaced by '@replacement'.",
        [
          '@plugin' => $plugin_id,
          '@replacement' => DataAggregatorBase::DEFAULT_STORAGE_CLIENT,
        ]
      )
    );
    \Drupal::logger('external_entities')->warning("Failed to load storage client plugin '$plugin_id'.");
    return DataAggregatorBase::DEFAULT_STORAGE_CLIENT;
  }

}
