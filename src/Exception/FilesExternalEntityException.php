<?php

namespace Drupal\external_entities\Exception;

/**
 * Exception thrown by External Entities Files Storage Client.
 */
class FilesExternalEntityException extends \RuntimeException {}
