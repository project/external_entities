<?php

namespace Drupal\external_entities\Entity;

use Drupal\external_entities\DataAggregator\DataAggregatorInterface;
use Drupal\external_entities\FieldMapper\FieldMapperInterface;

/**
 * Provides an interface defining an external entity type entity.
 */
interface ConfigurableExternalEntityTypeInterface extends ExternalEntityTypeInterface {

  /**
   * Get the fields that can be mapped.
   *
   * Computed fields are unmappable, which automatically excludes inherited
   * annotation fields as well. The annotation field is excluded as well.
   *
   * @param bool $reload
   *   If TRUE, reloads definitions.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   An associative array of field definitions, keyed by field name.
   */
  public function getMappableFields(bool $reload = FALSE) :array;

  /**
   * Get the fields that can be edited.
   *
   * Editable fields will be returned as TRUE values while read-only fields will
   * be returned as FALSE.
   *
   * @param bool $reload
   *   If TRUE, reloads the list.
   *
   * @return array
   *   An associative array of field name as keys and their editable status as
   *   boolean. TRUE means editable, FALSE means read-only.
   */
  public function getEditableFields(bool $reload = FALSE) :array;

  /**
   * Get the fields for which a mapping is required.
   *
   * @return string[]
   *   An array of field names of required fields.
   */
  public function getRequiredFields() :array;

  /**
   * Returns default values to create a field mapper.
   *
   * This configuration includes internal elements that must not be serialized
   * such as the reference to current external entity.
   *
   * @param string $field_name
   *   Name of the corresponding field.
   *
   * @return array
   *   The field mapper default configuration with internal elements.
   */
  public function getFieldMapperDefaultConfiguration(
    string $field_name,
  ) :array;

  /**
   * Retrieves the plugin ID of the given field mapper used for the given field.
   *
   * @param string $field_name
   *   The field machine name of which the field mapper is wanted.
   *
   * @return string
   *   The plugin ID of the field mapper. If the requested field field mapper is
   *   not set, returns an empty string.
   */
  public function getFieldMapperId(string $field_name) :string;

  /**
   * Sets the field mapper plugin ID to use for the given field.
   *
   * Sets the field mapper plugin and clears any previous config for this field
   * mapper.
   *
   * @param string $field_name
   *   The field machine name of which the field mapper should be set.
   * @param string $field_mapper_id
   *   The plugin ID of the field mapper.
   *
   * @return \Drupal\external_entities\Entity\ExternalEntityTypeInterface
   *   Returns current instance.
   */
  public function setFieldMapperId(
    string $field_name,
    string $field_mapper_id,
  ) :self;

  /**
   * Retrieves the field mapper for the given field.
   *
   * @param string $field_name
   *   The field machine name of which the field mapper is wanted.
   *
   * @return \Drupal\external_entities\FieldMapper\FieldMapperInterface
   *   The requested field mapper plugin.
   */
  public function getFieldMapper(string $field_name)
  :FieldMapperInterface|null;

  /**
   * Retrieves the configuration of this field mapper plugin.
   *
   * @param string $field_name
   *   The field machine name of which the field mapper is wanted.
   *
   * @return array
   *   An associative array with the field mapper configuration. If the
   *   requested mapper number has no config set, returns an empty array as
   *   default value.
   */
  public function getFieldMapperConfig(string $field_name) :array;

  /**
   * Sets the configuration of the given field mapper.
   *
   * @param string $field_name
   *   The field machine name of which the field mapper config should be set.
   * @param array $field_mapper_config
   *   The new configuration for the field mapper.
   *
   * @return \Drupal\external_entities\Entity\ExternalEntityTypeInterface
   *   Returns current instance.
   */
  public function setFieldMapperConfig(
    string $field_name,
    array $field_mapper_config,
  ) :self;

  /**
   * Retrieves the administrative notes for the field mapping.
   *
   * @return string
   *   The administratives notes of the field mapping.
   */
  public function getFieldMappingNotes() :string;

  /**
   * Sets the administrative notes for the field mapping.
   *
   * @param string $field_mapping_notes
   *   The administrative notes for the field mapping.
   *
   * @return \Drupal\external_entities\Entity\ExternalEntityTypeInterface
   *   Returns current instance.
   */
  public function setFieldMappingNotes(
    string $field_mapping_notes,
  ) :self;

  /**
   * Returns default values to create a data aggregator.
   *
   * This configuration includes internal elements that must not be serialized
   * such as the reference to current external entity.
   *
   * @return array
   *   The data aggregator default configuration with internal elements.
   */
  public function getDataAggregatorDefaultConfiguration() :array;

  /**
   * Retrieves the plugin ID of the data aggregator for this type.
   *
   * @return string
   *   The plugin ID of the data aggregator.
   */
  public function getDataAggregatorId() :string;

  /**
   * Sets the plugin ID of the data aggregator for this type.
   *
   * @param string $data_aggregator_id
   *   The plugin ID of the data aggregator.
   *
   * @return \Drupal\external_entities\Entity\ExternalEntityTypeInterface
   *   Returns current instance.
   */
  public function setDataAggregatorId(
    string $data_aggregator_id,
  ) :self;

  /**
   * Returns the data aggregator.
   *
   * If the selected data aggregator plugin id is not available, it falls back
   * to the default data aggregator (group aggregator).
   *
   * @return \Drupal\external_entities\DataAggregator\DataAggregatorInterface
   *   The data aggregator plugin.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   Thrown if nor the selected data aggregator plugin or the default one
   *   could be loaded.
   */
  public function getDataAggregator()
  :DataAggregatorInterface;

  /**
   * Retrieves the configuration of the data aggregator plugin.
   *
   * @return array
   *   An associative array with the data aggregator configuration.
   */
  public function getDataAggregatorConfig() :array;

  /**
   * Sets the configuration of the data aggregator plugin.
   *
   * @param array $data_aggregator_config
   *   The new configuration for the data aggregator.
   *
   * @return \Drupal\external_entities\Entity\ExternalEntityTypeInterface
   *   Returns current instance.
   */
  public function setDataAggregatorConfig(
    array $data_aggregator_config,
  ) :self;

  /**
   * Retrieves the administrative notes for the data aggregator.
   *
   * @return string
   *   The administratives notes of the data aggregator.
   */
  public function getDataAggregatorNotes() :string;

  /**
   * Sets the administrative notes for the data aggregator.
   *
   * @param string $data_aggregator_notes
   *   The administrative notes for the data aggregator.
   *
   * @return \Drupal\external_entities\Entity\ExternalEntityTypeInterface
   *   Returns current instance.
   */
  public function setDataAggregatorNotes(
    string $data_aggregator_notes,
  ) :self;

}
