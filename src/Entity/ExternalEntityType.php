<?php

namespace Drupal\external_entities\Entity;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\DefaultLazyPluginCollection;
use Drupal\external_entities\DataAggregator\DataAggregatorInterface;
use Drupal\external_entities\Event\ExternalEntitiesEvents;
use Drupal\external_entities\Event\ExternalEntityGetMappableFieldsEvent;
use Drupal\external_entities\FieldMapper\FieldMapperInterface;
use Psr\Log\LoggerInterface;

/**
 * Defines the external_entity_type entity.
 *
 * @ConfigEntityType(
 *   id = "external_entity_type",
 *   label = @Translation("External entity type"),
 *   handlers = {
 *     "list_builder" = "Drupal\external_entities\ExternalEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\external_entities\Form\ExternalEntityTypeForm",
 *       "edit" = "Drupal\external_entities\Form\ExternalEntityTypeForm",
 *       "delete" = "Drupal\external_entities\Form\ExternalEntityTypeDeleteForm",
 *     }
 *   },
 *   config_prefix = "external_entity_type",
 *   admin_permission = "administer external entity types",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   links = {
 *     "edit-form" = "/admin/structure/external-entity-types/{external_entity_type}",
 *     "delete-form" = "/admin/structure/external-entity-types/{external_entity_type}/delete",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "label_plural",
 *     "description",
 *     "content_class",
 *     "generate_aliases",
 *     "read_only",
 *     "debug_level",
 *     "field_mappers",
 *     "field_mapping_notes",
 *     "data_aggregator",
 *     "data_aggregator_notes",
 *     "persistent_cache_max_age",
 *     "annotation_entity_type_id",
 *     "annotation_bundle_id",
 *     "annotation_field_name",
 *     "inherits_annotation_fields"
 *  }
 * )
 */
class ExternalEntityType extends ConfigEntityBase implements ConfigurableExternalEntityTypeInterface, EntityWithPluginCollectionInterface {

  /**
   * Indicates that entities of this external entity type should not be cached.
   */
  const CACHE_DISABLED = 0;

  /**
   * Default field mapper plugin id.
   */
  const DEFAULT_FIELD_MAPPER = 'generic';

  /**
   * Default data aggregator plugin id.
   */
  const DEFAULT_DATA_AGGREGATOR = 'single';

  /**
   * The external entity type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the external entity type.
   *
   * @var string
   */
  protected $label;

  /**
   * The plural human-readable name of the external entity type.
   *
   * @var string
   */
  protected $label_plural;

  /**
   * The external entity type description.
   *
   * @var string
   */
  protected $description;

  /**
   * The external entity class used for content instances.
   *
   * @var string
   */
  protected $content_class = ExternalEntity::class;

  /**
   * Whether or not entity types of this external entity type are read only.
   *
   * @var bool
   */
  protected $read_only;

  /**
   * Debug level of current instance.
   *
   * @var int
   */
  protected $debug_level;

  /**
   * Whether or not to auto-generate aliases for this external entity type.
   *
   * @var bool
   */
  protected $generate_aliases;

  /**
   * Array of field mapper plugin settings.
   *
   * @var array
   */
  protected $field_mappers = [];

  /**
   * Field mapping administrative notes.
   *
   * @var string
   */
  protected $field_mapping_notes = '';

  /**
   * Array a mappable fields of this instance.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface[]
   */
  protected $mappableFields = [];

  /**
   * Field mapper plugin collection.
   *
   * @var \Drupal\Core\Plugin\DefaultLazyPluginCollection
   */
  protected $fieldMapperPluginCollection;

  /**
   * Data aggregator plugin settings.
   *
   * @var array
   */
  protected $data_aggregator = [];

  /**
   * Data aggregator plugin collection.
   *
   * @var \Drupal\Core\Plugin\DefaultLazyPluginCollection
   */
  protected $dataAggregatorCollection;

  /**
   * Data aggregator plugin instance.
   *
   * @var \Drupal\external_entities\DataAggregator\DataAggregatorInterface
   */
  protected $dataAggregator;

  /**
   * Data aggregator administrative notes.
   *
   * @var string
   */
  protected $data_aggregator_notes = '';

  /**
   * Max age entities of this external entity type may be persistently cached.
   *
   * @var int
   */
  protected $persistent_cache_max_age = self::CACHE_DISABLED;

  /**
   * The annotations entity type id.
   *
   * @var string
   */
  protected $annotation_entity_type_id;

  /**
   * The annotations bundle id.
   *
   * @var string
   */
  protected $annotation_bundle_id;

  /**
   * The field this external entity is referenced from by the annotation entity.
   *
   * @var string
   */
  protected $annotation_field_name;

  /**
   * Local cache for the annotation field.
   *
   * @var array
   *
   * @see ExternalEntityType::getAnnotationField()
   */
  protected $annotationField;

  /**
   * Indicates if the external entity inherits the annotation entity fields.
   *
   * @var bool
   */
  protected $inherits_annotation_fields = FALSE;

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return [
      'aggregator' => $this->getDataAggregatorPluginCollection(),
      'fieldmappers' => $this->getFieldMapperPluginCollection(),
    ];
  }

  /**
   * Returns the field mapper plugin collection.
   *
   * @return \Drupal\Core\Plugin\DefaultLazyPluginCollection
   *   The field mapper plugin collection.
   */
  public function getFieldMapperPluginCollection() {
    if (empty($this->fieldMapperPluginCollection)) {
      $fm_configs = [];
      foreach ($this->getMappableFields() as $field_name => $field_def) {
        $field_mapper = $this->getFieldMapperId($field_name);
        if (!empty($field_mapper)) {
          $fm_configs[$field_name] =
            ['id' => $field_mapper]
            + NestedArray::mergeDeep(
              $this->getFieldMapperDefaultConfiguration($field_name),
              $this->getFieldMapperConfig($field_name)
            );
        }
      }
      $this->fieldMapperPluginCollection = new DefaultLazyPluginCollection(
        \Drupal::service('plugin.manager.external_entities.field_mapper'),
        $fm_configs
      );
    }
    return $this->fieldMapperPluginCollection;
  }

  /**
   * Returns the data aggregator plugin collection.
   *
   * @return \Drupal\Core\Plugin\DefaultLazyPluginCollection
   *   The data aggregator plugin collection.
   */
  public function getDataAggregatorPluginCollection() {
    if (empty($this->dataAggregatorCollection)) {
      $ag_id = $this->getDataAggregatorId();
      $ag_config = [
        $ag_id =>
        ['id' => $ag_id]
        + NestedArray::mergeDeep(
          $this->getDataAggregatorDefaultConfiguration(),
          $this->getDataAggregatorConfig()
        ),
      ];
      $data_aggregator_manager = \Drupal::service('plugin.manager.external_entities.data_aggregator');
      $this->dataAggregatorCollection = new DefaultLazyPluginCollection(
        $data_aggregator_manager,
        $ag_config
      );
    }
    return $this->dataAggregatorCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function createDuplicate() {
    $clone = parent::createDuplicate();
    // Clear local plugin instances to make sure they won't be shared.
    $clone->dataAggregator = NULL;
    $clone->dataAggregatorCollection = NULL;
    $clone->fieldMapperPluginCollection = NULL;
    return $clone;
  }

  /**
   * {@inheritdoc}
   */
  public function getLogger() :LoggerInterface {
    return $this->logger($this->id() ?: 'external_entities');
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() :string {
    return $this->label ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getPluralLabel() :string {
    return $this->label_plural ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() :string {
    return $this->description ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function isReadOnly() :bool {
    return (bool) $this->read_only;
  }

  /**
   * {@inheritdoc}
   */
  public function getDebugLevel() :int {
    return $this->debug_level ?? 0;
  }

  /**
   * {@inheritdoc}
   */
  public function setDebugLevel(int $debug_level = 1) :self {
    $this->debug_level = $debug_level;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function automaticallyGenerateAliases() :bool {
    return (bool) $this->generate_aliases;
  }

  /**
   * {@inheritdoc}
   */
  public function getMappableFields(bool $reload = FALSE) :array {
    if (empty($this->mappableFields) || $reload) {
      $this->mappableFields = [];

      $derived_entity_type = $this->getDerivedEntityType();

      $fields = [];
      if (!empty($derived_entity_type)) {
        $fields = $this
          ->entityFieldManager()
          ->getFieldDefinitions(
            $derived_entity_type->id(),
            $derived_entity_type->id()
          );
      }
      $this->mappableFields = array_filter(
        $fields, function (FieldDefinitionInterface $field) {
          return $field->getName() !== ExternalEntityInterface::ANNOTATION_FIELD
            && !$field->isComputed();
        }
      );

      // Allow other modules to alter mappable fields.
      $event = new ExternalEntityGetMappableFieldsEvent(
        $this,
        $this->mappableFields
      );
      \Drupal::service('event_dispatcher')->dispatch(
        $event,
        ExternalEntitiesEvents::GET_MAPPABLE_FIELDS
      );

      $this->mappableFields = $event->getMappableFields();
    }

    return $this->mappableFields;
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableFields(bool $reload = FALSE) :array {
    $fields = $this->getMappableFields($reload);
    $editable_fields = [];
    if ($this->isReadOnly()) {
      $editable_fields = array_fill_keys(
        array_keys($fields),
        FALSE
      );
    }
    else {
      foreach (array_keys($fields) as $field_name) {
        $field_mapper = $this->getFieldMapper($field_name);
        if (empty($field_mapper)
          || !$field_mapper->couldReverseFieldMapping()
        ) {
          $editable_fields[$field_name] = FALSE;
        }
        else {
          $editable_fields[$field_name] = TRUE;
        }
      }
    }
    return $editable_fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredFields() :array {
    $fields = [
      'id',
      'title',
    ];

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldMapperDefaultConfiguration(
    string $field_name,
  ) :array {
    return [
      // Allow the mapper to call back into the entity type (e.g., to fetch
      // additional data like field lists from the remote service).
      ExternalEntityTypeInterface::XNTT_TYPE_PROP => $this,
      'field_name' => $field_name,
      'debug_level' => $this->getDebugLevel(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldMapperId(string $field_name) :string {
    // Update id from collection if available.
    if (!empty($this->fieldMapperPluginCollection)
      && ($this->fieldMapperPluginCollection->has($field_name))
    ) {
      $this->field_mappers[$field_name]['id'] =
        $this->fieldMapperPluginCollection->get($field_name)->getPluginId();
    }
    return $this->field_mappers[$field_name]['id'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function setFieldMapperId(
    string $field_name,
    string $field_mapper_id,
  ) :self {
    // Make sure field is mappable.
    $mappable_fields = $this->getMappableFields();
    if (!empty($field_name) && !empty($mappable_fields[$field_name])) {
      // Only update if changed.
      if ($field_mapper_id != ($this->field_mappers[$field_name]['id'] ?? '')) {
        // Update collection.
        if (!empty($this->fieldMapperPluginCollection)) {
          $this->fieldMapperPluginCollection->removeInstanceId($field_name);
          if (!empty($field_mapper_id)) {
            $this->fieldMapperPluginCollection->addInstanceId(
              $field_name,
              ['id' => $field_mapper_id]
              + $this->getFieldMapperDefaultConfiguration($field_name)
            );
          }
        }
        // Initialize the field mapper setting array if not already.
        $this->field_mappers[$field_name] = ['config' => []];
        // Set corresponding field mapper plugin.
        $this->field_mappers[$field_name]['id'] = $field_mapper_id;
      }
    }
    else {
      $this->getLogger()->warning(
        'Tried to set a field mapper on a non-mappable field ('
        . $field_name
        . ').'
      );
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldMapper(
    string $field_name,
  ) :FieldMapperInterface|null {
    // Make sure field is mappable.
    $mappable_fields = $this->getMappableFields();
    if (empty($field_name)) {
      $this->getLogger()->warning(
        'No field machine name provided!'
      );
      return NULL;
    }
    elseif (empty($mappable_fields[$field_name])) {
      $this->getLogger()->warning(
        "Given field ($field_name) is not mappable!"
      );
      return NULL;
    }
    elseif (empty($this->field_mappers[$field_name]['id'])) {
      // Not mapped.
      return NULL;
    }

    if (!$this->getFieldMapperPluginCollection()->has($field_name)) {
      return NULL;
    }

    return $this->fieldMapperPluginCollection->get($field_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldMapperConfig(string $field_name) :array {
    if (!empty($this->fieldMapperPluginCollection)
        && $this->fieldMapperPluginCollection->has($field_name)
    ) {
      $this->field_mappers[$field_name]['config'] =
        $this->fieldMapperPluginCollection->get($field_name)->getConfiguration();
      // Remove id used by the DefaultLazyPluginCollection.
      unset($this->field_mappers[$field_name]['config']['id']);
    }
    return $this->field_mappers[$field_name]['config'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function setFieldMapperConfig(
    string $field_name,
    array $field_mapper_config,
  ) :self {
    // Make sure field is mappable.
    $mappable_fields = $this->getMappableFields();
    if (!empty($field_name)
      && !empty($mappable_fields[$field_name])
      && !empty($this->field_mappers[$field_name]['id'])
    ) {
      // Update plugin or local config.
      // Note: local config is only used when no plugin instance is available.
      if (!empty($this->fieldMapperPluginCollection)
        && ($this->fieldMapperPluginCollection->has($field_name))
      ) {
        $this->fieldMapperPluginCollection->setInstanceConfiguration(
          $field_name,
          ['id' => $this->field_mappers[$field_name]['id']]
          + NestedArray::mergeDeep(
            $this->getFieldMapperDefaultConfiguration($field_name),
            $field_mapper_config
          )
        );
      }
      else {
        $this->field_mappers[$field_name]['config'] = $field_mapper_config;
      }
    }
    elseif (empty($this->field_mappers[$field_name]['id'])) {
      $this->getLogger()->warning(
        'Tried to configure mapping on a field ('
        . $field_name
        . ') that has no field mapper set.'
      );
    }
    else {
      $this->getLogger()->warning(
        'Tried to configure mapping on a non-mappable field ('
        . $field_name
        . ').'
      );
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldMappingNotes() :string {
    return $this->field_mapping_notes ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function setFieldMappingNotes(
    string $field_mapping_notes,
  ) :self {
    $this->field_mapping_notes = $field_mapping_notes;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDataAggregatorDefaultConfiguration() :array {
    return [
      // Allow the aggregator to call back into the entity type.
      ExternalEntityTypeInterface::XNTT_TYPE_PROP => $this,
      'debug_level' => $this->getDebugLevel(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getDataAggregatorId() :string {
    if (!empty($this->dataAggregator)) {
      $this->data_aggregator['id'] =
        $this->dataAggregator->getPluginId();
    }
    return $this->data_aggregator['id'] ?? static::DEFAULT_DATA_AGGREGATOR;
  }

  /**
   * {@inheritdoc}
   */
  public function setDataAggregatorId(
    string $data_aggregator_id,
  ) :self {
    // Only update if changed.
    if (!empty($data_aggregator_id)
      && ($data_aggregator_id != ($this->data_aggregator['id'] ?? ''))
    ) {
      // Update collection.
      if (!empty($this->dataAggregatorCollection)) {
        if (!empty($this->data_aggregator['id'])) {
          $this->dataAggregatorCollection->removeInstanceId($this->data_aggregator['id']);
        }
        if (!empty($data_aggregator_id)) {
          $this->dataAggregatorCollection->addInstanceId(
            $data_aggregator_id,
            ['id' => $data_aggregator_id]
            + $this->getDataAggregatorDefaultConfiguration()
          );
        }
      }
      // Update local properties.
      $this->data_aggregator['id'] = $data_aggregator_id;
      $this->data_aggregator['config'] = [];
      $this->dataAggregator = NULL;
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDataAggregator() :DataAggregatorInterface {
    if (empty($this->dataAggregator)) {
      $aggregator_id = $this->getDataAggregatorId();
      // Initialize plugin collection if needed.
      if (empty($this->dataAggregatorCollection)) {
        $this->getPluginCollections();
      }
      $this->dataAggregator =&
        $this->dataAggregatorCollection->get($aggregator_id);
    }
    return $this->dataAggregator;
  }

  /**
   * {@inheritdoc}
   */
  public function getDataAggregatorConfig() :array {
    // If we have a plugin instance, get its config.
    if (!empty($this->dataAggregator)) {
      $this->data_aggregator['config'] =
        $this->dataAggregator->getConfiguration();
      // Remove id used by the DefaultLazyPluginCollection.
      unset($this->data_aggregator['config']['id']);
    }
    return $this->data_aggregator['config'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function setDataAggregatorConfig(
    array $data_aggregator_config,
  ) :self {
    $data_aggregator_id = $this->getDataAggregatorId();
    if (!empty($data_aggregator_id)) {
      // Check if we got a local plugin instance lodaded already.
      // Note: local config is only used when no plugin instance is available.
      if (!empty($this->dataAggregatorCollection)) {
        $new_plugin_config =
          ['id' => $data_aggregator_id]
          + NestedArray::mergeDeep(
            $this->getDataAggregatorDefaultConfiguration(),
            $data_aggregator_config
          );
        $this->dataAggregatorCollection->setInstanceConfiguration(
          $data_aggregator_id,
          $new_plugin_config
        );
      }
      else {
        // Only set local config as no instance was loaded.
        $this->data_aggregator['config'] = $data_aggregator_config;
      }
    }
    else {
      $this->getLogger()->warning(
        'Tried to set data aggregator config with no data aggregator set.'
      );
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDataAggregatorNotes() :string {
    return $this->data_aggregator_notes ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function setDataAggregatorNotes(
    string $data_aggregator_notes,
  ) :self {
    $this->data_aggregator_notes = $data_aggregator_notes;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPersistentCacheMaxAge() :int {
    return $this->persistent_cache_max_age;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    // Clear the entity type definitions cache so changes flow through to the
    // related entity types.
    $this->entityTypeManager()->clearCachedDefinitions();

    // Clear the router cache to prevent RouteNotFoundException errors caused
    // by the Field UI module.
    \Drupal::service('router.builder')->rebuild();

    // Rebuild local actions so that the 'Add field' action on the 'Manage
    // fields' tab appears.
    \Drupal::service('plugin.manager.menu.local_action')->clearCachedDefinitions();

    // Clear the static and persistent cache.
    $storage->resetCache();
    if ($this->entityTypeManager()->hasDefinition($this->id())) {
      $this
        ->entityTypeManager()
        ->getStorage($this->id())
        ->resetCache();
    }

    $edit_link = $this->toLink(t('Edit entity type'), 'edit-form')->toString();
    if ($update) {
      $this->getLogger()->notice(
        'Entity type %label has been updated.',
        ['%label' => $this->label(), 'link' => $edit_link]
      );
    }
    else {
      // Notify storage to create the database schema.
      $entity_type = $this->entityTypeManager()->getDefinition($this->id());
      \Drupal::service('entity_type.listener')
        ->onEntityTypeCreate($entity_type);

      $this->getLogger()->notice(
        'Entity type %label has been added.',
        ['%label' => $this->label(), 'link' => $edit_link]
      );
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);
    \Drupal::service('entity_type.manager')->clearCachedDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivedEntityTypeId() :string {
    return $this->id() ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivedEntityType() :ContentEntityTypeInterface|null {
    return $this->entityTypeManager()->getDefinition($this->getDerivedEntityTypeId(), FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function isAnnotatable() :bool {
    return $this->getAnnotationEntityTypeId()
      && $this->getAnnotationBundleId()
      && $this->getAnnotationFieldName();
  }

  /**
   * {@inheritdoc}
   */
  public function getAnnotationEntityTypeId() :string|null {
    return $this->annotation_entity_type_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getAnnotationBundleId() :string|null {
    return $this->annotation_bundle_id ?: $this->getAnnotationEntityTypeId();
  }

  /**
   * {@inheritdoc}
   */
  public function getAnnotationFieldName() :string|null {
    return $this->annotation_field_name;
  }

  /**
   * Returns the entity field manager.
   *
   * @return \Drupal\Core\Entity\EntityFieldManagerInterface
   *   The entity field manager.
   */
  protected function entityFieldManager() :EntityFieldManagerInterface {
    return \Drupal::service('entity_field.manager');
  }

  /**
   * {@inheritdoc}
   */
  public function getAnnotationField() :FieldDefinitionInterface|null {
    if (!isset($this->annotationField) && $this->isAnnotatable()) {
      $field_definitions = $this->entityFieldManager()->getFieldDefinitions($this->getAnnotationEntityTypeId(), $this->getAnnotationBundleId());
      $annotation_field_name = $this->getAnnotationFieldName();
      if (!empty($field_definitions[$annotation_field_name])) {
        $this->annotationField = $field_definitions[$annotation_field_name];
      }
    }

    return $this->annotationField;
  }

  /**
   * {@inheritdoc}
   */
  public function inheritsAnnotationFields() :bool {
    return (bool) $this->inherits_annotation_fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getBasePath() :string {
    return str_replace('_', '-', strtolower($this->id));
  }

  /**
   * Gets the logger for a specific channel.
   *
   * @param string $channel
   *   The name of the channel.
   *
   * @return \Psr\Log\LoggerInterface
   *   The logger for this channel.
   */
  protected function logger($channel) :LoggerInterface {
    return \Drupal::getContainer()->get('logger.factory')->get($channel);
  }

  /**
   * {@inheritdoc}
   */
  public function get($property_name) {
    // Get "local" config data from plugins themselves as it may have changed.
    if ('field_mappers' == $property_name) {
      // Initialize plugin collection if needed.
      if (empty($this->fieldMapperPluginCollection)) {
        $this->getPluginCollections();
      }
      // Only update where we got plugins that may have altered config.
      foreach ($this->getFieldMapperPluginCollection() as $field_name => $field_mapper) {
        $fm_config = $field_mapper->getConfiguration();
        // Remove id used by the DefaultLazyPluginCollection.
        unset($fm_config['id']);
        // Clear debug level.
        unset($fm_config['debug_level']);
        $this->field_mappers[$field_name] = [
          'id' => $field_mapper->getPluginId(),
          'config' => $fm_config,
        ];
      }
    }
    elseif ('data_aggregator' == $property_name) {
      // Update with current data aggregator config.
      if (!empty($this->dataAggregator)) {
        $data_aggregator_config = $this->getDataAggregatorConfig();
        // Clear debug level.
        unset($data_aggregator_config['debug_level']);
        $this->data_aggregator = [
          'id' => $this->getDataAggregatorId(),
          'config' => $data_aggregator_config,
        ];
      }
    }
    return parent::get($property_name);
  }

  /**
   * {@inheritdoc}
   */
  public function set($property_name, $value) {
    // Update plugin configs ("local" config will be updated by parent).
    if ('field_mappers' == $property_name) {
      // Clear previous instances.
      if (!empty($this->fieldMapperPluginCollection)) {
        $this->fieldMapperPluginCollection->clear();
      }
      // Add new instances.
      foreach ($value as $field_name => $field_mapper_settings) {
        $this->setFieldMapperId($field_name, $field_mapper_settings['id'] ?? '');
        if (!empty($field_mapper_settings['id'])) {
          $this->setFieldMapperConfig($field_name, $field_mapper_settings['config'] ?? []);
        }
      }
    }
    elseif ('data_aggregator' == $property_name) {
      // Clear previous instances.
      if (!empty($this->dataAggregatorCollection)) {
        $this->dataAggregatorCollection->clear();
      }
      $this->setDataAggregatorId($value['id'] ?? '');
      if (!empty($value['id'])) {
        $this->setDataAggregatorConfig($value['config'] ?? []);
      }
    }
    return parent::set($property_name, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function __sleep(): array {
    // Prevent some properties from being serialized.
    return array_diff(parent::__sleep(), [
      'fieldMapperPluginCollection',
      'dataAggregator',
      'dataAggregatorCollection',
      'annotationField',
    ]);
  }

}
