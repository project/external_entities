<?php

namespace Drupal\external_entities\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\external_entities\Event\ExternalEntitiesEvents;
use Drupal\external_entities\Event\ExternalEntityExtractRawDataEvent;

/**
 * Defines the external entity class.
 *
 * @see external_entities_entity_type_build()
 */
class ExternalEntity extends ContentEntityBase implements ExternalEntityInterface {

  /**
   * Source raw data provided by the storage client.
   *
   * @var array
   */
  protected $rawData;

  /**
   * Returns external entity content entity base definition.
   *
   * @return array
   *   Base content entity definition.
   */
  public static function getBaseDefinition() {
    return [
      'id' => 'external_entity',
      'label' => t('External Entity'),
      'label_plural' => t('External Entities'),
      'label_collection' => t('External Entities'),
      'provider' => 'external_entities',
      'class' => 'Drupal\external_entities\Entity\ExternalEntity',
      'group' => 'content',
      'group_label' => t('Content'),
      'translatable' => TRUE,
      'admin_permission' => 'administer external entity types',
      'permission_granularity' => 'entity_type',
      'persistent_cache' => FALSE,
      'handlers' => [
        'storage' => 'Drupal\external_entities\ExternalEntityStorage',
        'view_builder' => 'Drupal\Core\Entity\EntityViewBuilder',
        'form' => [
          'default' => 'Drupal\external_entities\Form\ExternalEntityForm',
          'edit' => 'Drupal\external_entities\Form\ExternalEntityForm',
          'delete' => 'Drupal\Core\Entity\ContentEntityDeleteForm',
        ],
        'list_builder' => 'Drupal\external_entities\ExternalEntityListBuilder',
        'access' => 'Drupal\external_entities\ExternalEntityAccessControlHandler',
        'route_provider' => [
          'html' => 'Drupal\external_entities\Routing\ExternalEntityHtmlRouteProvider',
        ],
      ],
      'links' => [],
      'entity_keys' => [
        'id' => 'id',
        'uuid' => 'uuid',
        'label' => 'title',
        'langcode' => 'langcode',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $values,
    $entity_type,
    $bundle = FALSE,
    $translations = [],
  ) {
    $values['langcode'] = $values['langcode'] ?? [
      LanguageInterface::LANGCODE_DEFAULT => LanguageInterface::LANGCODE_NOT_SPECIFIED,
    ];
    parent::__construct($values, $entity_type, $bundle, $translations);
    $this->rawData = [];
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalEntityType() :ExternalEntityTypeInterface {
    return $this
      ->entityTypeManager()
      ->getStorage('external_entity_type')
      ->load($this->getEntityTypeId());
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    return self::defaultBaseFieldDefinitions();
  }

  /**
   * Provides the default base field definitions for external entities.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   An array of base field definitions for the entity type, keyed by field
   *   name.
   */
  public static function defaultBaseFieldDefinitions() {
    $fields = [];

    $fields['id'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('ID'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('UUID'))
      ->setReadOnly(TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Title'))
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'type' => 'string',
        'label' => 'hidden',
      ])
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(new TranslatableMarkup('Language'))
      ->setReadOnly(TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
    $fields = parent::bundleFieldDefinitions($entity_type, $bundle, $base_field_definitions);

    /** @var \Drupal\external_entities\Entity\ExternalEntityTypeInterface $external_entity_type */
    $external_entity_type = \Drupal::entityTypeManager()
      ->getStorage('external_entity_type')
      ->load($entity_type->id());
    if ($external_entity_type && $external_entity_type->isAnnotatable()) {
      // Add the annotation reference field.
      $fields[ExternalEntityInterface::ANNOTATION_FIELD] = BaseFieldDefinition::create('entity_reference')
        ->setLabel(t('Annotation'))
        ->setComputed(TRUE)
        ->setDescription(t('The annotation entity.'))
        ->setSetting('target_type', $external_entity_type->getAnnotationEntityTypeId())
        ->setSetting('handler', 'default')
        ->setSetting('handler_settings', [
          'target_bundles' => [$external_entity_type->getAnnotationBundleId()],
        ])
        ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'weight' => 5,
          'settings' => [
            'match_operator' => 'CONTAINS',
            'size' => '60',
            'placeholder' => '',
          ],
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayOptions('view', [
          'label' => t('Annotation'),
          'type' => 'entity_reference_label',
          'weight' => 0,
        ])
        ->setDisplayConfigurable('view', TRUE);

      // Have the external entity inherit its annotation fields.
      if ($external_entity_type->inheritsAnnotationFields()) {
        $inherited_fields = static::getInheritedAnnotationFields($external_entity_type);
        $field_prefix = ExternalEntityInterface::ANNOTATION_FIELD_PREFIX;
        foreach ($inherited_fields as $field) {
          $field_definition = BaseFieldDefinition::createFromFieldStorageDefinition($field->getFieldStorageDefinition())
            ->setName($field_prefix . $field->getName())
            ->setReadOnly(TRUE)
            ->setComputed(TRUE)
            ->setLabel($field->getLabel())
            ->setDisplayConfigurable('view', $field->isDisplayConfigurable('view'));
          $fields[$field_prefix . $field->getName()] = $field_definition;
        }
      }
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function toRawData() :array {
    // Not using $this->toArray() here because we don't want computed values.
    $entity_values = [];
    foreach ($this->getFields(FALSE) as $name => $property) {
      $entity_values[$name] = $property->getValue();
    }

    $storage = $this->entityTypeManager()->getStorage($this->getEntityTypeId());
    $raw_data = $storage->createRawDataFromEntityValues(
      $entity_values,
      $this->getOriginalRawData()
    );

    // Allow other modules to perform custom extraction logic.
    $event = new ExternalEntityExtractRawDataEvent($this, $raw_data);
    \Drupal::service('event_dispatcher')->dispatch(
      $event,
      ExternalEntitiesEvents::EXTRACT_RAW_DATA
    );

    return $event->getRawData();
  }

  /**
   * {@inheritdoc}
   */
  public function setOriginalRawData(array $raw_data) :self {
    $this->rawData = $raw_data;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOriginalRawData() :array {
    return $this->rawData;
  }

  /**
   * {@inheritdoc}
   */
  public function getAnnotation() :ContentEntityInterface|null {
    $external_entity_type = $this->getExternalEntityType();
    if ($external_entity_type->isAnnotatable()) {
      $properties = [
        $external_entity_type->getAnnotationFieldName() => $this->id(),
      ];

      $bundle_key = $this
        ->entityTypeManager()
        ->getDefinition($external_entity_type->getAnnotationEntityTypeId())
        ->getKey('bundle');
      if ($bundle_key) {
        $properties[$bundle_key] = $external_entity_type->getAnnotationBundleId();
      }

      $annotation = $this->entityTypeManager()
        ->getStorage($external_entity_type->getAnnotationEntityTypeId())
        ->loadByProperties($properties);
      if (!empty($annotation)) {
        return array_shift($annotation);
      }
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function mapAnnotationFields(?ContentEntityInterface $annotation = NULL) :ExternalEntityInterface {
    $external_entity_type = $this->getExternalEntityType();
    if ($external_entity_type->isAnnotatable()) {
      if (!$annotation) {
        $annotation = $this->getAnnotation();
      }

      if ($annotation) {
        $this->set(ExternalEntityInterface::ANNOTATION_FIELD, $annotation->id());
        if ($external_entity_type->inheritsAnnotationFields()) {
          $inherited_fields = static::getInheritedAnnotationFields($external_entity_type);
          $field_prefix = ExternalEntityInterface::ANNOTATION_FIELD_PREFIX;
          foreach ($inherited_fields as $field_name => $inherited_field) {
            $value = $annotation->get($field_name)->getValue();
            $this->set($field_prefix . $field_name, $value);
          }
        }
      }
    }

    return $this;
  }

  /**
   * Gets the fields that can be inherited by the external entity.
   *
   * @param \Drupal\external_entities\Entity\ExternalEntityTypeInterface $type
   *   The type of the external entity.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   An array of field definitions, keyed by field name.
   *
   * @see \Drupal\Core\Entity\EntityManagerInterface::getFieldDefinitions()
   */
  public static function getInheritedAnnotationFields(ExternalEntityTypeInterface $type) {
    $inherited_fields = [];

    $field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions($type->getAnnotationEntityTypeId(), $type->getAnnotationBundleId());
    foreach ($field_definitions as $field_name => $field_definition) {
      if ($field_name !== $type->getAnnotationFieldName()) {
        $inherited_fields[$field_name] = $field_definition;
      }
    }

    return $inherited_fields;
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheMaxAge() {
    return $this->getExternalEntityType()->getPersistentCacheMaxAge();
  }

}
