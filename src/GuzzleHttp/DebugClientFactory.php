<?php

namespace Drupal\external_entities\GuzzleHttp;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Site\Settings;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Utils;

/**
 * Helper class to construct a HTTP client with Drupal specific config.
 */
class DebugClientFactory {

  /**
   * The handler stack.
   *
   * @var \GuzzleHttp\HandlerStack
   */
  protected $stack;

  /**
   * Constructs a new ClientFactory instance.
   *
   * @param \GuzzleHttp\HandlerStack $stack
   *   The handler stack.
   */
  public function __construct(HandlerStack $stack) {
    $this->stack = $stack;
  }

  /**
   * Constructs a new client object from some configuration.
   *
   * @param array $config
   *   The config for the client.
   *
   * @return \GuzzleHttp\Client
   *   The HTTP client.
   */
  public function fromOptions(array $config = []) {
    if (method_exists(Utils::class, 'defaultUserAgent')) {
      // D10+.
      $guzzle_agent = Utils::defaultUserAgent();
    }
    elseif (function_exists('\GuzzleHttp\default_user_agent')) {
      // D8-D9.
      $guzzle_agent = \GuzzleHttp\default_user_agent();
    }
    else {
      // Fallback for future changes.
      $guzzle_agent = 'GuzzleHttp';
    }
    $default_config = [
      // Security consideration: we must not use the certificate authority
      // file shipped with Guzzle because it can easily get outdated if a
      // certificate authority is hacked. Instead, we rely on the certificate
      // authority file provided by the operating system which is more likely
      // going to be updated in a timely fashion. This overrides the default
      // path to the pem file bundled with Guzzle.
      'verify' => TRUE,
      'timeout' => 30,
      'headers' => [
        'User-Agent' => 'Drupal/' . \Drupal::VERSION . ' (+https://www.drupal.org/) ' . $guzzle_agent,
      ],
      'handler' => $this->stack,
      // Security consideration: prevent Guzzle from using environment variables
      // to configure the outbound proxy.
      'proxy' => [
        'http' => NULL,
        'https' => NULL,
        'no' => [],
      ],
    ];

    $config = NestedArray::mergeDeep($default_config, Settings::get('http_client_config', []), $config);

    return new DebugClient($config);
  }

}
