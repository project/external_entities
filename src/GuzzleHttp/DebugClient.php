<?php

namespace Drupal\external_entities\GuzzleHttp;

use Drupal\Core\Logger\LoggerChannelTrait;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

/**
 * Debug version of Guzzle Client that logs query info in Drupal logs.
 */
class DebugClient extends Client {

  use LoggerChannelTrait;

  /**
   * Debug level of current instance.
   *
   * @var int
   */
  protected $debugLevel = 1;

  /**
   * Default logger channel name.
   *
   * @var string
   */
  protected $defaultLoggerChannel = 'xntt_guzzle';

  /**
   * {@inheritdoc}
   */
  public function request($method, $uri = '', array $options = []): ResponseInterface {
    if (2 <= $this->getDebugLevel()) {
      $this->getLogger($this->defaultLoggerChannel)->debug(
        "METHOD: '$method'\nURI: '$uri'\nOPTIONS:\n{options}",
        ['options' => print_r($options, TRUE)]
      );
    }
    $response = parent::request($method, $uri, $options);
    $status = $response->getStatusCode();
    if (2 <= $this->getDebugLevel()) {
      $this->getLogger($this->defaultLoggerChannel)->debug(
        "STATUS CODE: "
        . $status
        . "\nHEADERS: {headers}",
        ['headers' => print_r($response->getHeaders(), TRUE)]
      );
      if (200 == $status) {
        $this->getLogger($this->defaultLoggerChannel)->debug(
          "BODY:\n{body}",
          ['body' => '' . $response->getBody()]
        );
      }
    }
    elseif (1 == $this->getDebugLevel()) {
      $this->getLogger($this->defaultLoggerChannel)->debug(
        "METHOD: '$method'\nURI: '$uri'\nSTATUS CODE: $status"
      );
    }

    return $response;
  }

  /**
   * Returns debug level for current instance.
   *
   * @return int
   *   The debug level. 0 means debugging is off.
   */
  public function getDebugLevel() :int {
    return $this->debugLevel ?? 0;
  }

  /**
   * Sets debug level for current instance.
   *
   * @param int $debug_level
   *   Sets the debug level. A non-0 value means debugging is enabled while 0
   *   disables  debugging. Default to 1 (enabled).
   */
  public function setDebugLevel(int $debug_level = 1) {
    $this->debugLevel = $debug_level;
  }

  /**
   * Sets the logger channel to use for logs.
   *
   * @param string $channel
   *   Channel name. Should be set to plugin channel name.
   */
  public function setDefaultLoggerChannel(string $channel) {
    $this->defaultLoggerChannel = $channel;
  }

}
