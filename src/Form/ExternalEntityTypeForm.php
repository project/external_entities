<?php

namespace Drupal\external_entities\Form;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\external_entities\DataAggregator\DataAggregatorInterface;
use Drupal\external_entities\Entity\ConfigurableExternalEntityTypeInterface;
use Drupal\external_entities\Entity\ExternalEntityType;
use Drupal\external_entities\FieldMapper\FieldMapperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the external entity type add and edit forms.
 */
class ExternalEntityTypeForm extends EntityForm {

  /**
   * Field name prefix used for external entity managed fields.
   */
  public const MANAGED_FIELD_PREFIX = 'xntt_';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The entity display repository service.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The field mapper manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $fieldMapperManager;

  /**
   * The data aggregator manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $dataAggregatorManager;

  /**
   * External entity type config restrictions.
   *
   * @var array
   * @see https://www.drupal.org/project/external_entities/issues/3500290
   */
  protected $lockStatus;

  /**
   * Constructs an ExternalEntityTypeForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $field_mapper_manager
   *   The field mapper manager.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $data_aggregator_manager
   *   The data aggregator manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    EntityDisplayRepositoryInterface $entity_display_repository,
    MessengerInterface $messenger,
    DateFormatterInterface $date_formatter,
    PluginManagerInterface $field_mapper_manager,
    PluginManagerInterface $data_aggregator_manager,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityDisplayRepository = $entity_display_repository;
    $this->messenger = $messenger;
    $this->dateFormatter = $date_formatter;
    $this->fieldMapperManager = $field_mapper_manager;
    $this->dataAggregatorManager = $data_aggregator_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_display.repository'),
      $container->get('messenger'),
      $container->get('date.formatter'),
      $container->get('plugin.manager.external_entities.field_mapper'),
      $container->get('plugin.manager.external_entities.data_aggregator')
    );
  }

  /**
   * Provides an edit external entity type title callback.
   *
   * @param string $entity_type_id
   *   The entity type id.
   *
   * @return string
   *   The title for the entity type edit page.
   */
  public function title($entity_type_id = NULL) {
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    return $entity_type->getLabel();
  }

  /**
   * {@inheritdoc}
   *
   * For external entity types created from config, it is possible to restrict
   * parts of the config form. To do so, you have to set an entry corresponding
   * to the given external entity type into the Drupal state
   * 'external_entities.type.locked' as follow:
   * @code
   *   $locked = \Drupal::state()->get('external_entities.type.locked');
   *   $locked[<your_xntt_type_id>] = [
   *     // Lock external entity editing.
   *     'lock_edit' => TRUE,
   *     // Prevent external entity from being removed.
   *     'lock_delete' => TRUE,
   *     // Disables field management.
   *     'lock_fields' => TRUE,
   *     // Disables field display management (including form display and field
   *     // management).
   *     'lock_display' => TRUE,
   *     // Disables annotation management.
   *     'lock_annotations' => TRUE,
   *     // To restrict field mapping options.
   *     'lock_field_mappers' => [
   *       [
   *         '<field_name1>' => [
   *           // To restrict field mapper selection.
   *           'allow_plugins' => [
   *             'generic' => 'generic',
   *             'text' => 'text',
   *           ],
   *           // To disable field mapping config changes.
   *           'lock_config' => TRUE,
   *           // To completely hide field mapping config.
   *           'hide_config' => TRUE,
   *         ],
   *         '<field_name2>' => [
   *           'allow_plugins' => [
   *             // Restrict the use of the 'text 'field mapper to field_name2.
   *             'text' => 'text',
   *           ],
   *         ],
   *         // For all fields (overrided by field-specific settings).
   *         '*' => [
   *           'allow_plugins' => [
   *             'generic' => 'generic',
   *           ],
   *           // To manage default field mapping config changes.
   *           'lock_config' => FALSE,
   *           // To manage default visibility of field mapping configs.
   *           'hide_config' => FALSE,
   *         ],
   *       ],
   *     ],
   *     'lock_data_aggregator' => [
   *     // To restrict type data aggregator selection.
   *       'allow_plugins' => [
   *         'group' => 'group',
   *       ],
   *       // To disable type data aggregator config changes.
   *       'lock_config' => FALSE,
   *       // To completely hide type data aggregator config.
   *       'hide_config' => FALSE,
   *       // To restrict storage clients.
   *       'lock_storage_clients' => [
   *         // Restriction for first storage client (string keys allowed).
   *         0 => [
   *           // To restrict type storage client selection.
   *           'allow_plugins' => [
   *             'rest' => 'rest',
   *           ],
   *           // To disable type storage client config changes.
   *           'lock_config' => TRUE,
   *           // To completely hide type storage client config.
   *           'hide_config' => FALSE,
   *         ],
   *       ],
   *     ],
   *   ];
   *   Drupal::state()->set('external_entities.type.locked', $locked);
   * @endcode
   */
  public function form(array $form, FormStateInterface $form_state) {
    $external_entity_type = $this->getEntity();

    // Check if external entity type has locks.
    // @todo see https://www.drupal.org/project/external_entities/issues/3500290
    $this->lockStatus = \Drupal::state()->get('external_entities.type.locked');
    if (!empty($this->lockStatus[$external_entity_type->id()]['lock_edit'])) {
      return [
        '#type' => 'markup',
        '#markup' => $this->t('This external entity type is not editable.'),
      ];
    }

    $form = parent::form($form, $form_state);
    $form['#attributes']['id'] ??= 'xntt_' . $external_entity_type->id();
    $form['#attached']['library'][] = 'external_entities/external_entities';

    if ($this->operation == 'add') {
      $form['#title'] = $this->t('Add external entity type');
    }
    else {
      $form['#title'] = $this->t('Edit %label external entity type', ['%label' => $external_entity_type->label()]);
    }

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#maxlength' => 255,
      '#default_value' => $external_entity_type->label(),
      '#description' => $this->t('The human-readable name of this external entity type. This name must be unique.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $external_entity_type->id(),
      '#machine_name' => [
        'exists' => [$this, 'exists'],
      ],
      '#maxlength' => EntityTypeInterface::ID_MAX_LENGTH,
      '#disabled' => !$external_entity_type->isNew(),
    ];

    $form['label_plural'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name (plural)'),
      '#maxlength' => 255,
      '#default_value' => $external_entity_type->getPluralLabel(),
      '#description' => $this->t('The plural human-readable name of this external entity type.'),
      '#required' => TRUE,
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t("Description of the external entity type."),
      '#default_value' => $external_entity_type->getDescription(),
    ];

    $form['read_only'] = [
      '#title' => $this->t('Read only'),
      '#type' => 'checkbox',
      '#default_value' => $external_entity_type->isReadOnly(),
      '#description' => $this->t('Whether or not this external entity type is read only.'),
    ];

    $form['debug_level'] = [
      '#title' => $this->t('Debug mode'),
      '#type' => 'select',
      '#options' => [
        0 => $this->t('Disabled'),
        1 => $this->t('Log events'),
        2 => $this->t('Log events with details'),
        8 => $this->t('Log events and do not perform altering operations'),
      ],
      '#default_value' => $external_entity_type->getDebugLevel() ?? 0,
      '#description' => $this->t('Enables debugging: it also sets data aggregator, storage client and field mapper to debug mode.'),
    ];

    $form['generate_aliases'] = [
      '#title' => $this->t('Automatically generate aliases'),
      '#type' => 'checkbox',
      '#default_value' => $external_entity_type->automaticallyGenerateAliases(),
      '#description' => $this->t('Whether or not to automatically generate aliases for this external entity type.'),
    ];

    $form['additional_settings'] = [
      '#type' => 'vertical_tabs',
    ];

    $form['field_mapping'] = [
      '#type' => 'details',
      '#title' => $this->t('Field mapping'),
      '#group' => 'additional_settings',
      '#open' => TRUE,
    ];

    if ($external_entity_type->isNew()) {
      $form['field_mapping']['message_new'] = [
        '#type' => 'item',
        '#markup' => $this->t('You must save this external entity type before being able to map fields.'),
      ];
    }
    else {
      $this->buildFieldMappingConfigForm($form, $form_state);
    }

    // Data aggregator and storage clients.
    $form['storages_tab'] = [
      '#type' => 'details',
      '#title' => $this->t('Storage'),
      '#group' => 'additional_settings',
      '#open' => FALSE,
      'id' => [],
      'config' => [
        '#type' => 'container',
        '#attributes' => [
          'id' => $form['#attributes']['id'] . '_da',
        ],
      ],
    ];

    $this->buildDataAggregatorSelectForm($form, $form_state);
    $this->buildDataAggregatorForm($form, $form_state);

    $form['caching'] = [
      '#type' => 'details',
      '#title' => $this->t('Caching'),
      '#group' => 'additional_settings',
      '#open' => FALSE,
    ];

    $period = [
      0,
      60,
      180,
      300,
      600,
      900,
      1800,
      2700,
      3600,
      10800,
      21600,
      32400,
      43200,
      86400,
    ];
    $period = array_map([$this->dateFormatter, 'formatInterval'], array_combine($period, $period));
    $period[ExternalEntityType::CACHE_DISABLED] = '<' . $this->t('no caching') . '>';
    $period[Cache::PERMANENT] = $this->t('Permanent');
    $persistent_cache_max_age = $form_state->getValue(['caching', 'entity_cache_max_age'], ExternalEntityType::CACHE_DISABLED)
        ?: $external_entity_type->getPersistentCacheMaxAge();
    $form['caching']['persistent_cache_max_age'] = [
      '#type' => 'select',
      '#title' => $this->t('Persistent cache maximum age'),
      '#description' => $this->t('The maximum time the external entity can be persistently cached.'),
      '#options' => $period,
      '#default_value' => $persistent_cache_max_age,
    ];

    $form['annotations'] = [
      '#type' => 'details',
      '#title' => $this->t('Annotations'),
      '#group' => 'additional_settings',
      '#open' => FALSE,
    ];

    // @todo see https://www.drupal.org/project/external_entities/issues/3500290
    if (!empty($this->lockStatus[$this->getEntity()->id()]['lock_annotations'])) {
      $form['annotations']['#disabled'] = TRUE;
    }

    $annotations_enable = $form_state->getValue(['annotations', 'annotations_enable']);
    if ($annotations_enable === NULL) {
      $annotations_enable = $external_entity_type->isAnnotatable();
    }
    $form['annotations']['annotations_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable annotations on this external entity type'),
      '#description' => $this->t('Annotations allow to enrich external entities with locally stored data (such as Drupal fields). This is achieved by linking local entities with external entities using an entity reference field (on the local entity). Before enabling this option, you have to make sure an entity reference field (to the external entity type) is available on the local entity type and/or bundle.'),
      '#default_value' => $annotations_enable,
      '#ajax' => [
        'callback' => [$this, 'refreshAnnotationSettings'],
        'wrapper' => 'annotation-settings-wrapper',
      ],
    ];

    $form['annotations']['annotation_settings'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'annotation-settings-wrapper',
      ],
    ];

    if ($annotations_enable) {
      $annotation_entity_type_id_options = $this->getAnnotationEntityTypeIdOptions();
      $annotation_entity_type_id = $form_state->getValue([
        'annotations',
        'annotation_settings',
        'annotation_entity_type_id',
      ]) ?: $external_entity_type->getAnnotationEntityTypeId();
      $form['annotations']['annotation_settings']['annotation_entity_type_id'] = [
        '#type' => 'select',
        '#title' => $this->t('Entity type'),
        '#description' => $this->t('Entity type used for annotations.'),
        '#options' => $annotation_entity_type_id_options,
        '#attributes' => [
          'autocomplete' => 'off',
        ],
        '#ajax' => [
          'callback' => [$this, 'refreshAnnotationBundleOptions'],
          'wrapper' => 'annotation-bundle-id-wrapper',

        ],
        '#default_value' => $annotation_entity_type_id,
        '#required' => TRUE,
      ];

      $annotation_bundle_id = $form_state->getValue([
        'annotations',
        'annotation_settings',
        'annotation_bundle_id',
      ]) ?: $external_entity_type->getAnnotationBundleId();
      $form['annotations']['annotation_settings']['annotation_bundle_id'] = [
        // Prefix closed by following 'annotation_field_name' #suffix.
        '#prefix' => '<div id="annotation-bundle-id-wrapper">',
        '#type' => 'select',
        '#title' => $this->t('Bundle'),
        '#description' => $this->t('Bundle used for annotations.'),
        '#options' => $this->getAnnotationBundleIdOptions($annotation_entity_type_id),
        '#attributes' => [
          'autocomplete' => 'off',
        ],
        '#ajax' => [
          'callback' => [$this, 'refreshAnnotationFieldOptions'],
          'wrapper' => 'annotation-field-config-id-wrapper',
        ],
        '#default_value' => $annotation_bundle_id,
        '#disabled' => !$annotation_entity_type_id,
        '#required' => TRUE,
      ];

      $annotation_field_name = $form_state->getValue([
        'annotations',
        'annotation_settings',
        'annotation_field_name',
      ]) ?: $external_entity_type->getAnnotationFieldName();
      $annotation_field_options = $this->getAnnotationFieldOptions($annotation_entity_type_id, $annotation_bundle_id);
      $annotation_field_default_value = !empty($annotation_field_options[$annotation_field_name])
        ? $annotation_field_options[$annotation_field_name]
        : NULL;
      $form['annotations']['annotation_settings']['annotation_field_name'] = [
        '#prefix' => '<div id="annotation-field-config-id-wrapper">',
        // Also closes 'annotation_bundle_id' #prefix.
        '#suffix' => '</div></div>',
        '#type' => 'select',
        '#title' => $this->t('Entity reference field'),
        '#description' => $this->t('The entity reference field on the annotation entity which references the external entity.'),
        '#options' => $annotation_field_options,
        '#default_value' => $annotation_field_default_value,
        '#disabled' => !$annotation_entity_type_id || !$annotation_bundle_id,
        '#required' => TRUE,
      ];

      $inherits_annotation_fields = $form_state->getValue([
        'annotations',
        'annotation_settings',
        'inherits_annotation_fields',
      ], FALSE) ?: $external_entity_type->inheritsAnnotationFields();
      $form['annotations']['annotation_settings']['inherits_annotation_fields'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Inherit fields'),
        '#description' => $this->t('When enabled, fields and their values are automatically transferred from the annotation to the external entity. Inherited fields are regular entity fields and can be used as such.'),
        '#default_value' => $inherits_annotation_fields,
      ];
    }

    $form['#tree'] = TRUE;

    return $form;
  }

  /**
   * AJAX callback which refreshes the annotation settings.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   *
   * @return array
   *   The form structure.
   */
  public function refreshAnnotationSettings(array $form) {
    return $form['annotations']['annotation_settings'] ?: [];
  }

  /**
   * AJAX callback which refreshes the annotation bundle options.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   *
   * @return array
   *   The form structure.
   */
  public function refreshAnnotationBundleOptions(array $form) {
    return [
      'annotations' => [
        'annotation_settings' => [
          'annotation_bundle_id' => $form['annotations']['annotation_settings']['annotation_bundle_id'] ?: [],
          'annotation_field_name' => $form['annotations']['annotation_settings']['annotation_field_name'] ?: [],
        ],
      ],
    ];
  }

  /**
   * AJAX callback which refreshes the annotation field options.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   *
   * @return array
   *   The form structure.
   */
  public function refreshAnnotationFieldOptions(array $form) {
    return $form['annotations']['annotation_settings']['annotation_field_name'] ?: [];
  }

  /**
   * Builds the field mapping configuration form.
   *
   * @todo API: Return $form array instead of using reference &$form.
   *
   * @param array $form
   *   The current form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   If a field mapper plugin fails to load.
   */
  public function buildFieldMappingConfigForm(array &$form, FormStateInterface $form_state) {
    $external_entity_type = $this->getEntity();

    // Get mappable fields.
    $fields = $external_entity_type->getMappableFields();
    foreach ($fields as $field) {
      // @todo We could get data aggregator field mapping requests here but what
      //   if the storage client has changed. We can't set field mappings to
      //   readonly without handling aggregator/storage client changes.
      $field_name = $field->getName();

      $required = in_array(
        $field_name,
        $external_entity_type->getRequiredFields()
      );

      $form['field_mapping'][$field_name] = [
        '#type' => 'details',
        '#title' => $this->t('%field field', ['%field' => $field->getLabel()]),
        '#required' => $required,
        '#open' => FALSE,
        '#attributes' => [
          'id' => $form['#attributes']['id'] . '_fm_' . $field_name,
        ],
        'id' => [],
        'config' => [
          '#type' => 'container',
          '#attributes' => [
            'id' => $form['#attributes']['id'] . '_fm_' . $field_name . '_conf',
          ],
        ],
      ];
      $this->buildFieldMapperSelectForm($form, $form_state, $field);
      $this->buildFieldMapperConfigForm($form, $form_state, $field);
    }
    // Add notes.
    $form['field_mapping']['notes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Notes'),
      '#description' => $this->t('Administrative notes for maintenance.'),
      '#default_value' => $external_entity_type->getFieldMappingNotes(),
    ];
  }

  /**
   * Builds the field mapper selection configuration.
   *
   * @todo API: Return $form array instead of using reference &$form.
   *
   * @param array $form
   *   The current form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_def
   *   The field definition.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   If a field mapper plug-in cannot be loaded.
   */
  public function buildFieldMapperSelectForm(
    array &$form,
    FormStateInterface $form_state,
    FieldDefinitionInterface $field_def,
  ) {
    $external_entity_type = $this->getEntity();
    $field_name = $field_def->getName();
    $field_mapper_options = [];

    $field_mappers = $this->fieldMapperManager->getCompatibleFieldMappers($field_def->getType());
    $current_field_mapper_id =
      $form_state->getValue(['field_mapping', $field_name, 'id'])
      ?? $external_entity_type->getFieldMapperId($field_name);

    foreach ($field_mappers as $field_mapper_id => $definition) {
      // Check for plugin restrictions.
      // @todo see https://www.drupal.org/project/external_entities/issues/3500290
      if ((!empty($this->lockStatus[$external_entity_type->id()]['lock_field_mappers'][$field_name]['allow_plugins'])
          && empty($this->lockStatus[$external_entity_type->id()]['lock_field_mappers'][$field_name]['allow_plugins'][$field_mapper_id]))
        || (empty($this->lockStatus[$external_entity_type->id()]['lock_field_mappers'][$field_name]['allow_plugins'])
          && !empty($this->lockStatus[$external_entity_type->id()]['lock_field_mappers']['*']['allow_plugins'])
          && empty($this->lockStatus[$external_entity_type->id()]['lock_field_mappers']['*']['allow_plugins'][$field_mapper_id]))
      ) {
        continue;
      }

      $config = [];
      if ($field_mapper_id === $current_field_mapper_id) {
        // This mapper was previously used; init it with saved config.
        $config = $external_entity_type->getFieldMapperConfig($field_name);
      }
      $config +=
        $external_entity_type->getFieldMapperDefaultConfiguration($field_name);

      $field_mapper = $this->fieldMapperManager->createInstance(
        $field_mapper_id,
        $config
      );
      $field_mapper_options[$field_mapper_id] = $field_mapper->getLabel();
    }

    if ($field_mapper_options) {
      $required = in_array(
        $field_name,
        $external_entity_type->getRequiredFields()
      );
      // Make sure we set a field mapper for required fields.
      if ($required
        && (empty($current_field_mapper_id)
          || empty($field_mapper_options[$current_field_mapper_id]))
      ) {
        // If only one option or default not available, use first option.
        if ((1 == count($field_mapper_options))
            || (empty($field_mapper_options[ExternalEntityType::DEFAULT_FIELD_MAPPER]))
        ) {
          $current_field_mapper_id = array_keys($field_mapper_options)[0];
        }
        else {
          // Use default.
          $current_field_mapper_id = ExternalEntityType::DEFAULT_FIELD_MAPPER;
        }
        $form_state->setValue(
          ['field_mapping', $field_name, 'id'],
          $current_field_mapper_id
        );
      }
      if ($required && (1 == count($field_mapper_options))) {
        // Only one compulsory option, hide the choice.
        $form['field_mapping'][$field_name]['id'] = [
          '#type' => 'hidden',
          '#value' => key($field_mapper_options),
        ];
      }
      else {
        $form['field_mapping'][$field_name]['id'] = [
          '#type' => 'select',
          '#title' => $this->t('Field mapper:'),
          '#options' => $field_mapper_options,
          '#sort_options' => TRUE,
          '#default_value' => $current_field_mapper_id,
          '#required' => $required,
          '#empty_value' => $required ? NULL : '',
          '#empty_option' => $required ? NULL : $this->t('Not mapped'),
          '#wrapper_attributes' => ['class' => ['xntt-inline']],
          '#attributes' => [
            'class' => ['xntt-field'],
            'autocomplete' => 'off',
          ],
          '#label_attributes' => ['class' => ['xntt-label']],
          '#ajax' => [
            'callback' => [get_class($this), 'buildAjaxFieldMapperConfigForm'],
            'wrapper' =>
            $form['field_mapping'][$field_name]['config']['#attributes']['id']
            ?? NULL,
            'method' => 'replaceWith',
            'effect' => 'fade',
          ],
        ];
      }
    }
  }

  /**
   * Builds a field mapper configuration form.
   *
   * @todo API: Return $form array instead of using reference &$form.
   *
   * @param array $form
   *   The current form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   If a field mapper plugin fails to load.
   */
  public function buildFieldMapperConfigForm(
    array &$form,
    FormStateInterface $form_state,
    FieldDefinitionInterface $field_definition,
  ) {
    $external_entity_type = $this->getEntity();
    $field_name = $field_definition->getName();
    // @todo Add the note properly.
    // @code
    // if ('uuid' == $field_name) {
    //   $form['field_mappings'][$field_name]['config']['note'] = [
    //     '#type' => 'item',
    //     '#markup' => $this->t('It is advised to map this field'),
    //   ];
    // }
    // @endcode

    try {
      // Get field mapper id from form or from config.
      $field_mapper_id =
        $form_state->getValue(['field_mapping', $field_name, 'id'])
        ?? $external_entity_type->getFieldMapperId($field_name);

      if ($field_mapper_id) {
        // Check for config restrictions.
        // @todo see https://www.drupal.org/project/external_entities/issues/3500290
        if (!empty($this->lockStatus[$external_entity_type->id()]['lock_field_mappers'][$field_name]['lock_config'])
          || (!array_key_exists('lock_config', $this->lockStatus[$external_entity_type->id()]['lock_field_mappers'][$field_name] ?? [])
            && !empty($this->lockStatus[$external_entity_type->id()]['lock_field_mappers']['*']['lock_config']))
        ) {
          $form['field_mapping'][$field_name]['#disabled'] = TRUE;
        }

        // The external entity can't be new here as it has been tested before.
        if ($field_mapper_id === $external_entity_type->getFieldMapperId($field_name)) {
          // This is the currently selected field mapper, use xntt instance.
          $field_mapper = $external_entity_type->getFieldMapper($field_name);
        }
        else {
          // Generate a new instance.
          $mapper_config =
            $external_entity_type->getFieldMapperDefaultConfiguration($field_name);
          $field_mapper = $this->fieldMapperManager->createInstance(
            $field_mapper_id,
            $mapper_config
          );
        }
        if ($field_mapper instanceof FieldMapperInterface) {
          // Make sure we got a default form structure.
          $form['field_mapping'][$field_name]['config'] ??= [
            '#type' => 'container',
            '#attributes' => [
              'id' => ($form['#attributes']['id'] ??= 'xntt_' . $external_entity_type->id())
              . '_fm_'
              . $field_name
              . '_conf',
            ],
          ];
          // Attach the field mapper plugin configuration form.
          $field_mapper_form_state = XnttSubformState::createForSubform(
            ['field_mapping', $field_name, 'config'],
            $form,
            $form_state
          );
          $form['field_mapping'][$field_name]['config'] =
            $field_mapper->buildConfigurationForm(
              $form['field_mapping'][$field_name]['config'],
              $field_mapper_form_state
            );
        }
      }

      // Check for config restrictions.
      // @todo see https://www.drupal.org/project/external_entities/issues/3500290
      if (!empty($this->lockStatus[$external_entity_type->id()]['lock_field_mappers'][$field_name]['hide_config'])
        || (!array_key_exists('hide_config', $this->lockStatus[$external_entity_type->id()]['lock_field_mappers'][$field_name] ?? [])
          && !empty($this->lockStatus[$external_entity_type->id()]['lock_field_mappers']['*']['hide_config']))
      ) {
        $form['field_mapping'][$field_name]['#type'] = 'hidden';
      }
    }
    catch (PluginException $e) {
      $form['field_mapping'][$field_name]['config'] = [
        '#type' => 'item',
        '#markup' => $this->t(
          'WARNING: Failed to load a field mapping plugin!'
        ),
      ];
      $this->logger('external_entities')->error(
        'Failed to load a field mapping plugin: '
        . $e
      );
    }
  }

  /**
   * Handles switching the selected field mapper plugin.
   *
   * @param array $form
   *   The current form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return array
   *   The part of the form to return as AJAX.
   */
  public static function buildAjaxFieldMapperConfigForm(array $form, FormStateInterface $form_state) {
    // The work is already done in form(), so we just need to return the
    // relevant part of the form here. Since the field name is not a parameter,
    // we have to go down two steps ('field_mapping'>'field_<field_name>') in
    // the form tree to find the wrapper element corresponding to our field and
    // return it.
    $triggering_element = $form_state->getTriggeringElement();
    $parents = $triggering_element['#array_parents'];

    return $form[$parents[0]][$parents[1]]['config'] ?? [];
  }

  /**
   * Builds the data aggregator selection configuration.
   *
   * @todo API: Return $form array instead of using reference &$form.
   *
   * @param array $form
   *   The current form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   If a data aggregator plug-in cannot be loaded.
   */
  public function buildDataAggregatorSelectForm(array &$form, FormStateInterface $form_state) {
    $data_aggregators = $this->dataAggregatorManager->getDefinitions();
    if (1 >= count($data_aggregators)) {
      return;
    }
    $external_entity_type = $this->getEntity();
    $data_aggregator_options = [];
    $data_aggregator_descriptions = [];
    $current_data_aggregator_id =
      $form_state->getValue(['storages_tab', 'id'])
      ?? $external_entity_type->getDataAggregatorId()
      ?? ExternalEntityType::DEFAULT_DATA_AGGREGATOR;

    foreach ($data_aggregators as $data_aggregator_id => $definition) {
      // Check for plugin restrictions.
      // @todo see https://www.drupal.org/project/external_entities/issues/3500290
      if (!empty($this->lockStatus[$external_entity_type->id()]['lock_data_aggregator']['allow_plugins'])
        && empty($this->lockStatus[$external_entity_type->id()]['lock_data_aggregator']['allow_plugins'][$data_aggregator_id])
      ) {
        continue;
      }

      $config = $external_entity_type->getDataAggregatorDefaultConfiguration();
      $data_aggregator = $this->dataAggregatorManager->createInstance($data_aggregator_id, $config);
      $data_aggregator_options[$data_aggregator_id] = $data_aggregator->getLabel();
      $data_aggregator_descriptions[$data_aggregator_id]['#description'] = $data_aggregator->getDescription();
    }
    asort($data_aggregator_options, SORT_NATURAL | SORT_FLAG_CASE);

    if ($data_aggregator_options) {
      $form['storages_tab']['id'] = [
        '#type' => 'select',
        '#title' => $this->t('Data aggregator:'),
        '#description' => $this->t('Choose a data aggregator to use for this type.'),
        '#options' => $data_aggregator_options,
        '#default_value' => $current_data_aggregator_id,
        '#required' => TRUE,
        '#wrapper_attributes' => ['class' => ['xntt-inline']],
        '#attributes' => [
          'class' => ['xntt-field'],
          'autocomplete' => 'off',
        ],
        '#label_attributes' => ['class' => ['xntt-label']],
        '#ajax' => [
          'callback' => [get_class($this), 'buildAjaxDataAggregatorConfigForm'],
          'wrapper' => $form['storages_tab']['config']['#attributes']['id'] ?? NULL,
          'method' => 'replaceWith',
          'effect' => 'fade',
        ],
      ];
      $form['storages_tab']['id'] += $data_aggregator_descriptions;
    }
  }

  /**
   * Builds the data aggregator configuration form.
   *
   * @todo API: Return $form array instead of using reference &$form.
   *
   * @param array $form
   *   The current form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public function buildDataAggregatorForm(
    array &$form,
    FormStateInterface $form_state,
  ) {
    $external_entity_type = $this->getEntity();
    $data_aggregator_id =
      $form_state->getValue(['storages_tab', 'id'])
      ?? $external_entity_type->getDataAggregatorId()
      ?? ExternalEntityType::DEFAULT_DATA_AGGREGATOR;

    if ($data_aggregator_id == $external_entity_type->getDataAggregatorId()) {
      $data_aggregator = $external_entity_type->getDataAggregator();
    }
    else {
      $config = [];
      // Special case for built-in data aggregators: config transfer because
      // configs are compatible.
      if (in_array($data_aggregator_id, ['single', 'group'])
        && in_array($external_entity_type->getDataAggregatorId(), ['single', 'group'])
      ) {
        // This aggregator was previously used; init it with saved config.
        $config = $external_entity_type->getDataAggregatorConfig();
      }
      $config += $external_entity_type->getDataAggregatorDefaultConfiguration();
      $data_aggregator = $this->dataAggregatorManager->createInstance(
        $data_aggregator_id,
        $config
      );
    }

    if ($data_aggregator instanceof DataAggregatorInterface) {
      // Make sure we got a default form structure.
      $form['storages_tab']['config'] ??= [
        '#type' => 'container',
        '#attributes' => [
          'id' => ($form['#attributes']['id'] ??= 'xntt_' . $external_entity_type->id()) . '_da',
        ],
      ];
      // Attach the data aggregator plugin configuration form.
      $data_aggregator_form_state = XnttSubformState::createForSubform(
        ['storages_tab', 'config'],
        $form,
        $form_state
      );
      $form['storages_tab']['config'] =
        $data_aggregator->buildConfigurationForm(
          $form['storages_tab']['config'],
          $data_aggregator_form_state
        );
      // Notes.
      $form['storages_tab']['notes'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Notes'),
        '#description' => $this->t('Administrative notes for maintenance.'),
        '#default_value' => $storage_settings['notes']
        ?? $external_entity_type->getDataAggregatorNotes(),
      ];

      // Check for plugin restrictions.
      // @todo see https://www.drupal.org/project/external_entities/issues/3500290
      if (!empty($this->lockStatus[$external_entity_type->id()]['lock_data_aggregator']['lock_config'])) {
        $form['storages_tab']['config']['#disabled'] = TRUE;
      }
      if (!empty($this->lockStatus[$external_entity_type->id()]['lock_data_aggregator']['hide_config'])) {
        $form['storages_tab']['config']['#type'] = 'hidden';
      }
    }
  }

  /**
   * Handles switching the selected data aggregator plugin.
   *
   * @param array $form
   *   The current form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return array
   *   The part of the form to return as AJAX.
   */
  public static function buildAjaxDataAggregatorConfigForm(
    array $form,
    FormStateInterface $form_state,
  ) {
    // The work is already done in form(), where we rebuild the entity according
    // to the current form values and then create the storage client
    // configuration form based on that. So we just need to return the relevant
    // part of the form
    // here.
    return $form['storages_tab']['config'];
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    // @todo see https://www.drupal.org/project/external_entities/issues/3500290
    if (!empty($this->lockStatus[$this->getEntity()->id()]['lock_edit'])) {
      // Not editable.
      return [];
    }

    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save external entity type');

    if (!$this->getEntity()->isNew()) {
      $actions['submit2'] = $actions['submit'];
      $actions['submit2']['#value'] = $this->t('Save & edit');
      $actions['submit2']['#name'] = 'save_and_edit';
    }

    // @todo see https://www.drupal.org/project/external_entities/issues/3500290
    if (!empty($this->lockStatus[$this->getEntity()->id()]['lock_delete'])) {
      // Not deletable.
      unset($actions['delete']);
    }

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $external_entity_type = $this->getEntity();
    parent::validateForm($form, $form_state);

    // Validate data aggregator settings.
    $data_aggregator_id = $form_state->getValue(
      ['storages_tab', 'id'],
      ExternalEntityType::DEFAULT_DATA_AGGREGATOR
    );
    $data_aggregator_config = $form_state->getValue(
      ['storages_tab', 'config'],
      []
    );
    $data_aggregator_config = NestedArray::mergeDeep(
      $external_entity_type->getDataAggregatorDefaultConfiguration(),
      $data_aggregator_config
    );

    $data_aggregator = $this->dataAggregatorManager->createInstance(
      $data_aggregator_id,
      $data_aggregator_config
    );
    if ($data_aggregator instanceof DataAggregatorInterface) {
      $data_aggregator_form_state = XnttSubformState::createForSubform(
        ['storages_tab', 'config'],
        $form,
        $form_state
      );
      $data_aggregator->validateConfigurationForm(
        $form['storages_tab']['config'],
        $data_aggregator_form_state
      );
    }

    // Validate field mapper settings.
    $fields = $external_entity_type->getMappableFields();
    foreach ($fields as $field) {
      $field_name = $field->getName();
      $field_mapper_id = $form_state->getValue(
        ['field_mapping', $field_name, 'id']
      );
      if ($field_mapper_id) {
        $mapper_config =
          $external_entity_type->getFieldMapperDefaultConfiguration($field_name);
        $field_mapper = $this->fieldMapperManager->createInstance(
          $field_mapper_id,
          $mapper_config
        );
        if ($field_mapper instanceof PluginFormInterface) {
          $field_mapper_form_state = XnttSubformState::createForSubform(
            ['field_mapping', $field_name, 'config'],
            $form,
            $form_state
          );
          $field_mapper->validateConfigurationForm(
            $form['field_mapping'][$field_name]['config'],
            $field_mapper_form_state
          );
        }
      }
    }

    // If rebuild needed, ignore validation.
    if ($form_state->isRebuilding()) {
      $form_state->clearErrors();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $external_entity_type = $this->getEntity();
    $requested_fields = [];
    // Submit and save all storage clients and aggregation settings.
    $data_aggregator_id = $form_state->getValue(
      ['storages_tab', 'id'],
      ExternalEntityType::DEFAULT_DATA_AGGREGATOR
    );
    $data_aggregator_config =
      $external_entity_type->getDataAggregatorDefaultConfiguration();
    $external_entity_type->setDataAggregatorId($data_aggregator_id);
    $external_entity_type->setDataAggregatorConfig($data_aggregator_config);
    $data_aggregator = $external_entity_type->getDataAggregator();
    if ($data_aggregator instanceof DataAggregatorInterface) {
      $data_aggregator_form_state = XnttSubformState::createForSubform(
        ['storages_tab', 'config'],
        $form,
        $form_state
      );
      $data_aggregator->submitConfigurationForm(
        $form['storages_tab']['config'],
        $data_aggregator_form_state
      );
      $da_config = $data_aggregator->getConfiguration();
      // Get requested fields from data aggregator.
      $requested_fields = $data_aggregator->getRequestedDrupalFields();
    }

    // $form_state->clearValue('storages_tab');
    $form_state->setValue(
      'data_aggregator',
      [
        'id' => $data_aggregator_id,
        'config' => $da_config,
      ]
    );

    if ($external_entity_type instanceof ConfigurableExternalEntityTypeInterface) {
      $fields = $external_entity_type->getMappableFields();
      $entity_type_id = $external_entity_type->id();

      // Remove unnecessary fields.
      foreach ($fields as $field_name => $field_definition) {
        // Managed fields have a machine name starting with FIELD_PREFIX.
        if (str_starts_with($field_name, static::MANAGED_FIELD_PREFIX)
            && (!array_key_exists($field_name, $requested_fields))
        ) {
          // Remove Drupal field.
          $field_config_id =
            $entity_type_id
            . '.'
            . $entity_type_id
            . '.'
            . $field_name;
          $field_config = $this->entityTypeManager
            ->getStorage('field_config')
            ->load($field_config_id);
          if (!empty($field_config)) {
            try {
              $field_config->delete();
              $this->messenger->addMessage(
                $this->t(
                  'Unused field %label was removed.',
                  ['%label' => $field_name]
                )
              );
            }
            catch (\Exception $e) {
              $error = TRUE;
              $this->messenger->addError(
                $this->t(
                  'There was a problem removing field %label: @message',
                  ['%label' => $field_name, '@message' => $e->getMessage()]
                )
              );
            }
          }
        }
      }
      // Add requested missing fields.
      foreach ($requested_fields as $field_name => $field_request) {
        if (array_key_exists('config', $field_request)
            && !array_key_exists($field_name, $fields)
            && (str_starts_with($field_name, static::MANAGED_FIELD_PREFIX)
                || str_starts_with($field_name, 'field_'))
            && !empty($field_request['required'])
            && in_array($field_request['required'], ['required', 'requested'])
        ) {
          try {
            $field_type = $field_request['type'] ?? 'string';
            $field_storage_config = $this
              ->entityTypeManager
              ->getStorage('field_storage_config')
              ->load($entity_type_id . '.' . $field_name);
            if (empty($field_storage_config)) {
              $field_storage_config = $this
                ->entityTypeManager
                ->getStorage('field_storage_config')
                ->create([
                  'field_name' => $field_name,
                  'entity_type' => $entity_type_id,
                  'type' => $field_type,
                ])
                ->save();
            }
            $field_config =
              [
                'field_name' => $field_name,
                'entity_type' => $entity_type_id,
                'bundle' => $entity_type_id,
              ]
              + $field_request['config'];
            $this
              ->entityTypeManager
              ->getStorage('field_config')
              ->create($field_config)
              ->save();
            $this->messenger->addMessage(
              $this->t(
                'New field %label was added.',
                ['%label' => $field_name]
              )
            );
            // Set field displayed in form and view.
            $this->entityDisplayRepository
              ->getFormDisplay($entity_type_id, $entity_type_id, 'default')
              ->setComponent($field_name, $field_request['form_display'] ?? [])
              ->save();
            $this->entityDisplayRepository
              ->getViewDisplay($entity_type_id, $entity_type_id)
              ->setComponent($field_name, $field_request['view_display'] ?? [])
              ->save();
          }
          catch (\Exception $e) {
            $error = TRUE;
            $this->messenger->addError(
              $this->t(
                'There was a problem creating field %label: @message',
                ['%label' => $field_name, '@message' => $e->getMessage()]
              )
            );
          }
        }
      }
      // Clear field cache.
      $fields = $external_entity_type->getMappableFields(TRUE);

      $field_mappers = [];
      $override_warnings = [];
      // Submit and save field mapper.
      foreach ($fields as $field) {
        $field_name = $field->getName();
        $field_type = $field->getType();

        // Get submitted values first and override after if needed.
        $field_mapper_id = $form_state->getValue(
          ['field_mapping', $field_name, 'id']
        );
        if (!empty($field_mapper_id)) {
          $mapper_config =
            $external_entity_type->getFieldMapperDefaultConfiguration($field_name);
          $external_entity_type->setFieldMapperId($field_name, $field_mapper_id);
          $external_entity_type->setFieldMapperConfig($field_name, $mapper_config);
          $field_mapper = $external_entity_type->getFieldMapper($field_name);
          if ($field_mapper instanceof PluginFormInterface) {
            $field_mapper_form_state = XnttSubformState::createForSubform(
              ['field_mapping', $field_name, 'config'],
              $form, $form_state
            );
            $field_mapper->submitConfigurationForm(
              $form['field_mapping'][$field_name]['config'],
              $field_mapper_form_state
            );
            $field_mappers[$field_name] = [
              'id' => $field_mapper_id,
              'config' => $field_mapper->getConfiguration(),
            ];
          }
        }
        else {
          $field_mappers[$field_name] = [
            'id' => NULL,
            'config' => [],
          ];
        }

        // Get requested field mappings from data aggregator.
        $requested_mapping = $data_aggregator->getRequestedMapping(
          $field_name,
          $field_type
        );

        // Check if required (override user input).
        if (isset($requested_mapping['id'])
            && isset($requested_mapping['config'])
        ) {
          if (!empty($requested_mapping['required'])
              && ('required' == $requested_mapping['required'])
          ) {
            // Override user input with storage client mapping.
            $mappings_differ = FALSE;
            if (empty($field_mappers[$field_name]['id'])) {
              // No user input, just use storage client mapping.
              $field_mappers[$field_name] = [
                'id' => $requested_mapping['id'],
                'config' => $requested_mapping['config'],
              ];
            }
            elseif ($field_mappers[$field_name]['id'] != $requested_mapping['id']) {
              // Different field mappers, replace all.
              $field_mappers[$field_name] = [
                'id' => $requested_mapping['id'],
                'config' => $requested_mapping['config'],
              ];
              $mappings_differ = TRUE;
            }
            else {
              foreach ($requested_mapping['config']['property_mappings'] as $property => $mapping) {
                // Get submitted property mapping and remove keys that should
                // not be compared.
                $prop_map_config = $field_mappers[$field_name]['config']['property_mappings'][$property]['config'] ?? [];
                unset($prop_map_config['required_field']);
                unset($prop_map_config['main_property']);
                unset($prop_map_config['description']);
                if (!isset($mapping['config']['data_processors']) && empty($prop_map_config['data_processors'])) {
                  unset($prop_map_config['data_processors']);
                }
                if (!empty($field_mappers[$field_name]['config']['property_mappings'][$property]['id'])
                  && ($field_mappers[$field_name]['config']['property_mappings'][$property]['id'] != $mapping['id']
                      || $this->arrayDiffer($prop_map_config, $mapping['config'])
                  )
                ) {
                  $mappings_differ = TRUE;
                }
                $field_mappers[$field_name]['config']['property_mappings'][$property] = $mapping;
              }
            }
            // Warn the user for overrides.
            if ($mappings_differ) {
              $override_warnings[] = $field_name;
            }
          }
          else {
            // Only override missing.
            $field_mappers[$field_name] ??= [];
            $field_mappers[$field_name]['id'] ??= $requested_mapping['id'];
            if (empty($field_mappers[$field_name]['config'])) {
              $field_mappers[$field_name]['config'] = $requested_mapping['config'];
            }
            else {
              foreach ($requested_mapping['config']['property_mappings'] as $property => $mapping) {
                if (empty($field_mappers[$field_name]['config']['property_mappings'][$property]['id'])) {
                  $field_mappers[$field_name]['config']['property_mappings'][$property] = $mapping;
                }
              }
            }
          }
        }
      }
      if (!empty($override_warnings)) {
        $this->messenger->addWarning(
          $this->t(
            'The submitted mapping for the following field(s) has been overriden by a storage client: %fields',
            ['%fields' => implode(', ', $override_warnings)]
          )
        );
      }
      // "field_mappers" is the external entity property to save.
      $form_state->setValue('field_mappers', $field_mappers);
    }

    // Save other parameters.
    $form_state->setValue(
      'field_mapping_notes',
      $form_state->getValue(['field_mapping', 'notes'], '')
    );
    $form_state->setValue(
      'data_aggregator_notes',
      $form_state->getValue(['storages_tab', 'notes'], '')
    );
    $form_state->setValue(
      'persistent_cache_max_age',
      (int) $form_state->getValue(['caching', 'persistent_cache_max_age'],
      ExternalEntityType::CACHE_DISABLED)
    );
    $form_state->setValue('annotation_entity_type_id', $form_state->getValue([
      'annotations',
      'annotation_settings',
      'annotation_entity_type_id',
    ], NULL));
    $form_state->setValue('annotation_bundle_id', $form_state->getValue([
      'annotations',
      'annotation_settings',
      'annotation_bundle_id',
    ], NULL));
    $form_state->setValue('annotation_field_name', $form_state->getValue([
      'annotations',
      'annotation_settings',
      'annotation_field_name',
    ], NULL));
    $form_state->setValue('inherits_annotation_fields', $form_state->getValue([
      'annotations',
      'annotation_settings',
      'inherits_annotation_fields',
    ], FALSE));

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $external_entity_type = $this->getEntity();
    $status = $external_entity_type->save();
    if ($status) {
      // Check if mapping is set.
      $fm = $external_entity_type->getFieldMapper('id');
      // Only redirect if ID mapping is set with regular save button.
      if (!empty($fm) && !empty($fm->getPropertyMapping('value')['id'])) {
        // Warn that the id field should be mapped in a way we can get a source
        // field name (required for many features and some storage clients).
        $this->messenger->addMessage($this->t('Saved the %label external entity type.', [
          '%label' => $external_entity_type->label(),
        ]));
        if (($trigger = $form_state->getTriggeringElement())
          && ($trigger['#name'] == 'save_and_edit')
        ) {
          // Ignore "destination" query parameter.
          $request = \Drupal::request();
          $request->query->remove('destination');
          // Redirect to current page to clear form state properly.
          $form_state->setRedirect(\Drupal::routeMatch()->getRouteName());
        }
        else {
          $form_state->setRedirect('entity.external_entity_type.collection');
        }
      }
      else {
        $this->messenger->addMessage($this->t('Saved the %label external entity type. Please now set a mapping for the required fields such as the entity identifier field.', [
          '%label' => $external_entity_type->label(),
        ]));
        // Redirect to external entity type edition page.
        $form_state->setRedirect(
          'entity.external_entity_type.edit_form',
          ['external_entity_type' => $external_entity_type->getDerivedEntityTypeId()],
          ['fragment' => 'edit-field-mapper-field-mapper-config-field-mappings-id-value']
        );
      }
    }
    else {
      $this->messenger->addMessage($this->t('The %label external entity type was not saved.', [
        '%label' => $external_entity_type->label(),
      ]));
    }
    return $status;
  }

  /**
   * Checks if the entity type already exists.
   *
   * @param string $entity_type_id
   *   The entity type id to check.
   *
   * @return bool
   *   TRUE if already exists, FALSE otherwise.
   */
  public function exists($entity_type_id) {
    if ($this->entityTypeManager->getDefinition($entity_type_id, FALSE)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Gets the form entity.
   *
   * The form entity which has been used for populating form element defaults.
   *
   * @return \Drupal\external_entities\Entity\ExternalEntityTypeInterface
   *   The current form entity.
   */
  public function getEntity() {
    /** @var \Drupal\external_entities\Entity\ExternalEntityTypeInterface $entity */
    $entity = $this->entity;
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    if ($route_match->getParameter($entity_type_id) !== NULL) {
      $entity_id = $route_match->getParameter($entity_type_id);
      $entity = $this->entityTypeManager->getStorage($entity_type_id)->load($entity_id);
      if ($entity) {
        return $entity;
      }
    }

    return parent::getEntityFromRouteMatch($route_match, $entity_type_id);
  }

  /**
   * Gets the annotation entity type id options.
   *
   * @return array
   *   Associative array of entity type labels, keyed by their ids.
   */
  public function getAnnotationEntityTypeIdOptions() {
    $options = [];

    $definitions = $this->entityTypeManager->getDefinitions();
    /** @var \Drupal\Core\Entity\EntityTypeInterface $definition */
    foreach ($definitions as $entity_type_id => $definition) {
      if ($entity_type_id !== 'external_entity' && $definition instanceof ContentEntityType) {
        $options[$entity_type_id] = $definition->getLabel();
      }
    }

    return $options;
  }

  /**
   * Gets the annotation bundle options.
   *
   * @param string $entity_type_id
   *   (optional) The entity type id.
   *
   * @return array
   *   Associative array of bundle labels, keyed by their ids.
   */
  public function getAnnotationBundleIdOptions($entity_type_id = NULL) {
    $options = [];

    if ($entity_type_id) {
      $bundles = $this->entityTypeBundleInfo->getBundleInfo($entity_type_id);
      foreach ($bundles as $bundle_id => $bundle) {
        $options[$bundle_id] = $bundle['label'];
      }
    }

    return $options;
  }

  /**
   * Gets the annotation field options.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param string $bundle_id
   *   The bundle id.
   *
   * @return array
   *   Associative array of field labels, keyed by their ids.
   */
  public function getAnnotationFieldOptions($entity_type_id, $bundle_id) {
    $options = [];

    if ($entity_type_id && $bundle_id) {
      $fields = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle_id);
      foreach ($fields as $id => $field) {
        if ($field->getType() === 'entity_reference' && $field->getSetting('target_type') === $this->getEntity()->getDerivedEntityTypeId()) {
          $options[$id] = $field->getLabel() . ' (' . $field->getName() . ')';
        }
      }
    }

    return $options;
  }

  /**
   * Tells if 2 associative arrays differ (recursively).
   *
   * @param array $a1
   *   First array.
   * @param array $a2
   *   Second array.
   *
   * @return bool
   *   TRUE if arrays differ. FALSE if they appear equal.
   */
  public function arrayDiffer(array $a1, array $a2) :bool {
    $differ = FALSE;
    if (count($a1) != count($a2)) {
      return TRUE;
    }
    foreach ($a1 as $key => $value) {
      if (!array_key_exists($key, $a2)) {
        $differ = TRUE;
        break;
      }
      if (is_array($value)) {
        if (!is_array($a2[$key])) {
          $differ = TRUE;
          break;
        }
        $differ = $this->arrayDiffer($value, $a2[$key]);
      }
      elseif ($value != $a2[$key]) {
        $differ = TRUE;
        break;
      }
    }
    return $differ;
  }

}
