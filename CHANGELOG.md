External Entities 3.0.0 beta 2, 2024-11-25
-------------------------------------------
- Improved documentation
- Multiple fixes in config schemas
- External entity list link added to Drupal Config page
- Added examples for JSON:API storage client
- Fixed multiple redirection issues on external entity type form saving
- Changed how queries are tracked for query limitations
- Query limitation is provided by a trait rather than RestClient
- Allowed to alter sorts through event subscriber
- Changed return type declaration of ExternalEntityType::getLogger() to
  LoggerInterface
- Allowed automatic field mapping by storage clients
- Added horizontal and vertical data aggregators
- Drupal 11 compatibility

External Entities 3.0.0 beta 1, 2024-09-13
-------------------------------------------
- Fixed multiple issues on file client base
- Improved Drupal-side filtering and limitations
- Removed invalid warning when creating a new xntt type
- Fixed missing dependencies
- Compatible storage client configs are transfered when switching storage
  clients or data aggregators
- Added tests for data processors
- Improved SQL database filter mapping
- Improved transliterate event
- Added xntt_views as experimental companion module
- Refactoring in form elements

External Entities 3.0.0 alpha 6, 2024-08-23
-------------------------------------------
- Beta 1 candidate
- Fixes multiple issues of External Entities v2
- Introduces multilingual support
- Added support for programmatically created external entity types
- Improved support for complex entity queries (sub-queries and "OR")
- Added native support for file and image field types (companion module)
- Added new base classes for storage clients to support files and query
  languages
- Improved user interface (xntt type list link, save without redirection,
  mapping interface for new external entity types)
- Improved support for data saving back to source storage (keep non-mapped
  values, allows reverse data processing)
- Introduced the data aggregator plugin to mix storage sources
- Replaced field mapping based on a single field mapper plugin for all the
  Drupal fields and their properties by 3 plugins: a new field mapper plugin
  that can be selected on a Drupal field basis, a property mapper plugin that
  can be selected on a field property basis and a data processor plugin that can
  be used to process source field data before its use (data processor can be
  chained on each property).
- Added debug features for non-developers (using logs).
- Improved test coverage
